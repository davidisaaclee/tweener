#!/usr/bin/env bash

xcodebuild build \
	-workspace 'Tweener.xcworkspace' \
	-scheme 'Tweener' \
	-destination 'platform=iOS,id=9fd82b1608c5136547e8936d36337c2109fccff9' \
	-derivedDataPath build

ios-deploy --debug --bundle build/Build/Products/Debug-iphoneos/Tweener.app/
