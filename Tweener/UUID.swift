import Foundation

extension UUID: Codable {
	init?(coder: NSCoder) {
		guard let uuidString = coder.decodeObject(forKey: "uuidString") as? String else {
			return nil
		}

		self.init(uuidString: uuidString)
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.uuidString, forKey: "uuidString")
	}
}
