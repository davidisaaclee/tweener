import Foundation

extension ComposerController: LayoutEditorDelegate {
	func layoutEditorDidTapPaletteButton(_ layoutEditor: LayoutEditorViewController) {
		let objectPalette = self.makeObjectPalette()
		let wrappedObjectPalette =
			type(of: self.navigationController).make(with: objectPalette)
		wrappedObjectPalette.modalPresentationStyle = .overFullScreen
		wrappedObjectPalette.modalTransitionStyle = .crossDissolve
		self.navigationController.fork(with: wrappedObjectPalette)
	}

	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didBeginTouchingObjectWithID objectID: UUID) {
		if case .layout = self.workspace.mode {
			self.workspace.saveToHistory()
		}
		
		self.workspace.touchedObjectIDs.insert(objectID)
	}

	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didDragObjectWithID objectID: UUID,
	                  dragAmount: CGPoint) {
		switch workspace.mode {
		case .overdub:
			self.workspace.canvasBuffer.objects =
				self.workspace.canvasBuffer.objects
					.withTransformedValue(at: objectID) { $0.moved(by: dragAmount) }

		case .playback, .recording:
			self.workspace.canvasBuffer.objects =
				self.workspace.canvasBuffer.objects
					.withTransformedValue(at: objectID) { $0.moved(by: dragAmount) }

		case .layout:
			self.workspace
				.modifyObjectInCurrentKeyframe(forID: objectID,
				                               transform: { $0?.moved(by: dragAmount) })

		case .editInteractions, .scrub, .tapped, .moveScrubPoint, .moveTapPoint, .interact:
			break
		}
	}

	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didEndTouchingObjectWithID objectID: UUID) {
		switch self.workspace.mode {
		case .recording, .overdub:
			// "Latch" on recording modes.
			break

		default:
			self.workspace.touchedObjectIDs.remove(objectID)
		}
	}

	func layoutEditorDidBeginRecording(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.saveToHistory()

		if self.workspace.currentTransition == nil {
			if let nextTransition = self.workspace.transitions(for: self.workspace.currentKey).first {
				self.workspace.currentTransitionID = nextTransition.id
			} else {
				let newTransition = self.workspace.addTransition(from: workspace.currentKeyID)
				self.workspace.currentTransitionID = newTransition.id
			}
		}

		guard let currentAnimation = workspace.currentTransition?.animation else {
			print("Could not add or get current transition's animation.")
			return
		}

		if currentAnimation.frames.isEmpty {
			self.workspace.mode = .recording(state: RecordingState())
		} else {
			if case .layout = self.workspace.mode {
				self.workspace.playback = Playback(fps: 60, position: 0)
			}
			self.workspace.mode = .overdub
		}
	}

	func layoutEditorDidEndRecording(_ layoutEditor: LayoutEditorViewController) {
		func finish(at position: Float, fps: Double) {
			self.workspace.playback =
				Playback(fps: fps,
				         position: position)

			self.workspace.touchedObjectIDs.removeAll()
			self.workspace.mode = .playback
		}

		switch self.workspace.mode {
		case let .recording(recordingState):
			finish(at: 0,
			       fps: recordingState.fps)

		case .overdub:
			guard let playback = self.workspace.playback else {
				fatalError()
			}

			finish(at: playback.position,
			       fps: playback.fps)

		case .editInteractions, .scrub, .tapped, .moveScrubPoint, .moveTapPoint, .playback, .layout, .interact:
			break
		}
	}

	func layoutEditorDidRequestSlowerPlayback(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.saveToHistory()
		self.workspace.modifyCurrentAnimation { $0.timescaled(byFactorOf: -1) }
	}

	func layoutEditorDidRequestFasterPlayback(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.saveToHistory()
		self.workspace.modifyCurrentAnimation { $0.timescaled(byFactorOf: 1) }
	}

	func layoutEditorDidToggleClick(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.saveToHistory()
		self.workspace.isClickEnabled = !self.workspace.isClickEnabled
	}

	func layoutEditorDidRequestEnterScrubMode(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.saveToHistory()

		self.workspace.mode = .interact
		self.workspace.playback?.position = 0
	}

	func layoutEditorDidRequestResumePlayback(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.mode = .playback
		self.workspace.playback = Playback(fps: 60, position: 0)
	}

	func layoutEditorDidRequestGoToKeyframe(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.saveToHistory()
		self.workspace.mode = .layout
	}

	func layoutEditorDidRequestGoBack(_ layoutEditor: LayoutEditorViewController) {
		self.goToParent()
	}

	func layoutEditorDidRequestUndo(_ layoutEditor: LayoutEditorViewController) {
		self.workspace.undo()
	}

	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didRequestDeleteObjectWithID objectID: Object.ID) {
		self.workspace.removeObjectFromKey(objectID: objectID)
		self.workspace.touchedObjectIDs.remove(objectID)
	}
}
