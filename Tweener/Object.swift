import Foundation
import UIKit
import VectorSwift

struct k {
	static let defaultFont = UIFont(name: "Helvetica", size: 80)!
}

struct Object {
	typealias ID = UUID

	enum Content {
		case text(String, UIFont)

		var boundingBox: CGRect {
			switch self {
			case let .text(string, font):
				return CGRect(origin: .zero,
				              size: NSString(string: string).size(attributes: [NSFontAttributeName: font]))
			}
		}
	}

	// Currently only tweens between positions
	static func tween(between object1: Object, and object2: Object, amount: Float) -> Object {
		var tweenedObject = object1
		let displacement = object2.position - object1.position
		tweenedObject.position =
			object1.position + displacement * CGFloat(amount)
		return tweenedObject
	}

	let id: ID

	var content: Object.Content
	var position: CGPoint
	var isHidden: Bool

	var transform: CGAffineTransform {
		return CGAffineTransform(translationX: self.position.x, y: self.position.y)
	}

	var boundingBox: CGRect {
		return self.content.boundingBox.applying(self.transform)
	}

	init(id: ID = UUID(),
	     content: Object.Content,
	     position: CGPoint,
	     isHidden: Bool = false) {
		self.id = id
		self.content = content
		self.position = position
		self.isHidden = isHidden
	}

	struct Diff {
		// Currently only tweens between positions
		static func tween(from fromDiff: Object.Diff,
		                  to toDiff: Object.Diff,
		                  amount: Float) -> Object.Diff {
			let displacement = toDiff.position - fromDiff.position
			return Object.Diff(objectID: fromDiff.objectID,
			                   position: fromDiff.position + displacement * CGFloat(amount),
			                   isHidden: toDiff.isHidden)
		}

		let objectID: Object.ID
		let position: CGPoint
		let isHidden: Bool

		init(objectID: Object.ID, position: CGPoint, isHidden: Bool) {
			self.objectID = objectID
			self.position = position
			self.isHidden = isHidden
		}

		init(base: Object, target: Object) {
			self.objectID = base.id
			self.position = target.position - base.position
			self.isHidden = target.isHidden
		}

		func apply(to base: Object) -> Object {
			var object = base
			object.position = base.position + self.position
			object.isHidden = self.isHidden
			return object
		}
	}
}

extension Object: Hashable {
	var hashValue: Int {
		return self.id.hashValue
	}

	static func == (lhs: Object, rhs: Object) -> Bool {
		// TODO: Make better
		return lhs.hashValue == rhs.hashValue
	}
}

extension Object: Transformable {
	func moved(by amount: CGPoint) -> Object {
		var copy = self
		copy.position = CGPoint(x: copy.position.x + amount.x,
		                        y: copy.position.y + amount.y)
		return copy
	}
}

extension Object: Codable {
	init?(coder: NSCoder) {
		guard
			let id: UUID = coder.decode(forKey: "id"),
			let content: Object.Content = coder.decode(forKey: "content")
			else {
				return nil
			}

		let position = coder.decodeCGPoint(forKey: "position")
		let isHidden = coder.decodeBool(forKey: "isHidden")

		self.init(id: id, content: content, position: position, isHidden: isHidden)
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.id, forKey: "id")
		coder.encode(self.content, forKey: "content")
		coder.encode(self.position, forKey: "position")
		coder.encode(self.isHidden, forKey: "isHidden")
	}
}

extension Object.Content: Codable {
	init?(coder: NSCoder) {
		guard
			let associatedData = coder.decodeObject(forKey: "associatedData") as? Data,
			let typeTag = coder.decodeObject(forKey: "typeTag") as? String
			else {
				return nil
			}

		let subunarchiver = NSKeyedUnarchiver(forReadingWith: associatedData)

		switch typeTag {
		case "text":
			guard
				let string = subunarchiver.decodeObject(forKey: "string") as? String,
				let font = subunarchiver.decodeObject(forKey: "font") as? UIFont
				else {
					return nil
				}

			self = .text(string, font)

		default:
			return nil
		}
	}

	func encode(with coder: NSCoder) {
		let associatedData = NSMutableData()
		let subarchiver = NSKeyedArchiver(forWritingWith: associatedData)

		switch self {
		case let .text(string, font):
			subarchiver.encode(string, forKey: "string")
			subarchiver.encode(font, forKey: "font")
		}

		subarchiver.finishEncoding()

		coder.encode(associatedData, forKey: "associatedData")

		var typeTag: String {
			switch self {
			case .text:
				return "text"
			}
		}

		coder.encode(typeTag, forKey: "typeTag")
	}
}

extension Object.Diff: Codable {
	init?(coder: NSCoder) {
		guard let objectID: Object.ID = coder.decode(forKey: "objectID") else {
			return nil
		}

		self.init(objectID: objectID,
		          position: coder.decodeCGPoint(forKey: "position"),
		          isHidden: coder.decodeBool(forKey: "isHidden"))
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.objectID, forKey: "objectID")
		coder.encode(self.position, forKey: "position")
		coder.encode(self.isHidden, forKey: "isHidden")
	}
}
