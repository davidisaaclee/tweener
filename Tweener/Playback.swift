import Foundation

struct Playback {
	var fps: Double

	/// Playhead position within the animation at the current time,
	/// as a number between 0 and 1.
	var position: Float

	// Defaults to "now": the first time a `Playback` is created, it was "last
	// updated" at initialization.
	private var lastUpdated: Date = Date()

	func updated(at time: Date, animation: Animation, isPlaying: Bool) -> Playback {
		var copy = self
		copy.lastUpdated = time

		if isPlaying {
			guard !animation.frames.isEmpty else {
				return copy
			}

			let timeSinceLastUpdate = time.timeIntervalSince(self.lastUpdated)
			let positionDelta =
				Float(self.fps * timeSinceLastUpdate) / Float(animation.frames.count)
			copy.position =
				(self.position + positionDelta).truncatingRemainder(dividingBy: 1.0)
		}

		return copy
	}

	func updated(at time: Date, startedAt: Date, animation: Animation) -> Playback {
		var copy = self
		copy.lastUpdated = time

		guard !animation.frames.isEmpty else {
			return copy
		}

		let elapsed = time.timeIntervalSince(startedAt)
		let animationDuration = Double(animation.frames.count) / self.fps
		let position = elapsed / animationDuration

		copy.position =
			Float(position).truncatingRemainder(dividingBy: 1.0)

		return copy
	}

	init(fps: Double, position: Float) {
		self.position = position
		self.fps = fps
	}
}


extension Playback: Codable {
	init?(coder: NSCoder) {
		self.init(fps: coder.decodeDouble(forKey: "fps"),
		          position: coder.decodeFloat(forKey: "position"))
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.fps, forKey: "fps")
		coder.encode(self.position, forKey: "position")
	}
}



// Things that need both an Animation and a Playback.
// (I don't where to put these.)

extension Animation {
	func duration(whenPlayedWith playback: Playback) -> TimeInterval {
		return TimeInterval(self.frames.count) / playback.fps
	}

	func frameIndex(for playback: Playback) -> Int {
		return self.frameIndex(atPosition: playback.position)
	}

	func frame(for playback: Playback) -> Animation.Frame {
		return self.frames[self.frameIndex(for: playback)]
	}

	func interpolationInfo(for playback: Playback) -> Animation.Interpolation {
		guard !self.frames.isEmpty else {
			return Animation.Interpolation(startFrame: Animation.Frame(diffs: [:]),
			                               endFrame: Animation.Frame(diffs: [:]),
			                               progress: 0)
		}

		let fractionFrameIndex = self.fractionFrameIndex(for: playback)
		let lowerFrameIndex = Int(floor(fractionFrameIndex))

		let startFrame =
			self.frames[lowerFrameIndex]
		let endFrame =
			self.frames[(lowerFrameIndex + 1) % self.frames.count]
		let progress =
			fractionFrameIndex.truncatingRemainder(dividingBy: 1.0)

		return Animation.Interpolation(startFrame: startFrame,
		                               endFrame: endFrame,
		                               progress: progress)
	}

	private func fractionFrameIndex(for playback: Playback) -> Float {
		guard frames.count > 0 else {
			return 0
		}

		return playback.position * Float(self.frames.count - 1)
	}

}
