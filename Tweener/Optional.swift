import Foundation

extension Optional {
	@discardableResult func tap(block: (Wrapped) -> Void) -> Optional {
		if let wrapped = self {
			block(wrapped)
		}

		return self
	}

	@discardableResult func otherwise(_ block: () -> Void) -> Optional {
		if self == nil {
			block()
		}

		return self
	}
}
