import CoreGraphics

protocol Transformable {
	func moved(by amount: CGPoint) -> Self
}
