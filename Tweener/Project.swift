import Foundation

struct Project {
	var name: String
	let dateCreated: Date
	var dateModified: Date
	var pathRelativeToProjectDirectory: String
	let workspaceID: Workspace.ID
}

extension Project: Codable {
	init?(coder: NSCoder) {
		guard
			let name = coder.decodeObject(forKey: "name") as? String,
			let dateCreated = coder.decodeObject(forKey: "dateCreated") as? Date,
			let dateModified = coder.decodeObject(forKey: "dateModified") as? Date,
			let path = coder.decodeObject(forKey: "path") as? String,
			let workspaceID: Workspace.ID = coder.decode(forKey: "workspaceID")
			else {
				return nil
			}

		self.init(name: name, 
		          dateCreated: dateCreated,
		          dateModified: dateModified,
		          pathRelativeToProjectDirectory: path,
		          workspaceID: workspaceID)
	}

	func encode(with coder: NSCoder) {
		coder.encode(name, forKey: "name")
		coder.encode(dateCreated, forKey: "dateCreated")
		coder.encode(dateModified, forKey: "dateModified")
		coder.encode(pathRelativeToProjectDirectory, forKey: "path")
		coder.encode(workspaceID, forKey: "workspaceID")
	}
}

struct ProjectListing {
	var projects: [Workspace.ID: Project]
}

extension ProjectListing: Codable {
	init?(coder: NSCoder) {
		guard let projects: [Project] = coder.decodeArray(forKey: "projects") else {
			return nil
		}

		let projectsDict: [Workspace.ID: Project] =
			projects.reduce([:]) { $0.appending(($1.workspaceID, $1)) }

		self.init(projects: projectsDict)
	}

	func encode(with coder: NSCoder) {
		coder.encode(Array(projects.values), forKey: "projects")
	}
}
