import Foundation
import BrightFutures

class ProjectDatabase {

	static let shared = ProjectDatabase()

	enum Error: Swift.Error {
		case failedToCreateFile
		case failedToDecodeData
		case external(Swift.Error)

		var localizedDescription: String {
			switch self {
			case .failedToCreateFile:
				return "Failed to create file"

			case .failedToDecodeData:
				return "Failed to decode data"

			case let .external(error):
				return "External error: \(error.localizedDescription)"
			}
		}
	}

	private let fileManager = FileManager.default

	private var projectListing = ProjectListing(projects: [:])

	func createNewProject(name: String) -> Future<(Project, Workspace), ProjectDatabase.Error> {
		let workspace = Workspace()
		return self._save(workspace)
			.map { (pathComponent) -> (Project, Workspace) in
				let project = Project(name: name,
				                      dateCreated: Date(),
				                      dateModified: Date(),
				                      pathRelativeToProjectDirectory: pathComponent,
				                      workspaceID: workspace.id)

				return (project, workspace)
			}
			// TODO: This could fail asynchronously, after completion of the returned future.
			.andThen { result in
				switch result {
				case let .success((project, workspace)):
					self.projectListing.projects[workspace.id] = project
					self.write(self.projectListing)
						.onFailure { print("Failed to write project directory: \($0.localizedDescription)") }

				default:
					break
				}
			}
	}

	/// Returns the path component of the saved workspace, relative to the
	/// project directory.
	func save(_ workspace: Workspace, as project: Project) -> Future<String, ProjectDatabase.Error> {
		guard workspace.id == project.workspaceID else {
			fatalError("Attempted to save workspace under project for a different workspace.")
		}

		return self._save(workspace)
			.onSuccess { url in
				self.projectListing.projects[workspace.id] = project
				self.write(self.projectListing)
					.onFailure { print("Failed to write project directory: \($0.localizedDescription)") }
			}
	}

	func loadWorkspace(for project: Project) -> Future<Workspace, ProjectDatabase.Error> {
		do {
			let url = try createProjectsDirectoryIfNeeded()
				.appendingPathComponent(project.pathRelativeToProjectDirectory)

			let data = try Data(contentsOf: url)
			return loadWorkspace(from: data)
		} catch {
			return Future<Workspace, ProjectDatabase.Error>(error: .external(error))
		}
	}

	func deleteWorkspace(for project: Project) -> Future<URL, ProjectDatabase.Error> {
		do {
			let workspaceURL = try createProjectsDirectoryIfNeeded()
				.appendingPathComponent(project.pathRelativeToProjectDirectory)
			return self.deleteWorkspace(at: workspaceURL)
				.onSuccess { _ in
					self.projectListing.projects.removeValue(forKey: project.workspaceID)
					self.write(self.projectListing)
			}
		} catch {
			return Future<URL, ProjectDatabase.Error>(error: .external(error))
		}
		
	}


	// MARK: Helpers

	/**
	Saves just the workspace to its own file.

	Returns the path component of the saved workspace, relative to the project
	directory.
	*/
	private func _save(_ workspace: Workspace) -> Future<String, ProjectDatabase.Error> {
		var workspace = workspace
		workspace.observers.removeAll()
		workspace.mode = .layout
		workspace.playback = nil
		workspace.touchedObjectIDs.removeAll()

		let promise = Promise<String, ProjectDatabase.Error>()

		let data = NSKeyedArchiver.archivedData(withRootCodable: workspace)

		do {
			let projectPathComponent = pathComponent(forWorkspaceFor: workspace.id)
			let fileURL = try createProjectsDirectoryIfNeeded()
				.appendingPathComponent(projectPathComponent)
			try fileManager.saveOrAppend(data, to: fileURL)

			promise.success(projectPathComponent)
		} catch {
			promise.failure(.external(error))
		}

		return promise.future
	}

	private func loadWorkspace(from data: Data) -> Future<Workspace, ProjectDatabase.Error> {
		let promise = Promise<Workspace, ProjectDatabase.Error>()

		if let workspace: Workspace = NSKeyedUnarchiver.unarchiveCodable(with: data) {
			promise.success(workspace)
		} else {
			promise.failure(.failedToDecodeData)
		}

		return promise.future
	}

	fileprivate func deleteWorkspace(at url: URL) -> Future<URL, ProjectDatabase.Error> {
		let promise = Promise<URL, ProjectDatabase.Error>()

		do {
			try fileManager.removeItem(at: url)
			promise.success(url)
		} catch {
			promise.failure(.external(error))
		}

		return promise.future
	}

	func getProjectListing() -> Future<ProjectListing, ProjectDatabase.Error> {
		return refreshProjectListing()
	}


	private func createProjectsDirectoryIfNeeded() throws -> URL {
		let documentsURL =
			try fileManager.url(for: .documentDirectory,
			                    in: .userDomainMask,
			                    appropriateFor: nil,
			                    create: true)

		let projectsURL =
			documentsURL.appendingPathComponent("projects", isDirectory: true)

		if !fileManager.fileExists(atPath: projectsURL.path) {
			try fileManager.createDirectory(at: projectsURL,
			                                withIntermediateDirectories: false,
			                                attributes: nil)
		}

		return projectsURL
	}

	private func pathComponent(forWorkspaceFor id: Workspace.ID) -> String {
		return id.uuidString
	}

	private func urlForProjectDirectory() throws -> URL {
		let projectsURL =
			try createProjectsDirectoryIfNeeded()
		return projectsURL.appendingPathComponent("Projects")
	}

	@discardableResult private func refreshProjectListing() -> Future<ProjectListing, ProjectDatabase.Error> {
		do {
			if fileManager.fileExists(atPath: try urlForProjectDirectory().path) {
				// Update the existing project directory file.
				return self.readProjectDirectory()
					.onSuccess { self.projectListing = $0 }
			} else {
				// Write a new project directory file from this object's project directory.
				return self.write(self.projectListing)
					.map { _ in return self.projectListing }
			}
		} catch {
			return Future<ProjectListing, ProjectDatabase.Error>(error: .external(error))
		}
	}

	private func readProjectDirectory() -> Future<ProjectListing, ProjectDatabase.Error> {
		let promise = Promise<ProjectListing, ProjectDatabase.Error>()

		do {
			let data = try Data(contentsOf: try urlForProjectDirectory())
			if let projectListing: ProjectListing = NSKeyedUnarchiver.unarchiveCodable(with: data) {
				promise.success(projectListing)
			} else {
				promise.failure(.failedToDecodeData)
			}
		} catch {
			promise.failure(.external(error))
		}

		return promise.future
	}

	@discardableResult private func write(_ projectDirectory: ProjectListing) -> Future<URL, ProjectDatabase.Error> {
		let promise = Promise<URL, ProjectDatabase.Error>()

		let data = NSKeyedArchiver.archivedData(withRootCodable: projectDirectory)

		do {
			try fileManager.saveOrAppend(data,
			                             to: try self.urlForProjectDirectory())
		} catch {
			promise.failure(.external(error))
		}

		return promise.future
	}

}

extension FileManager {
	func saveOrAppend(_ data: Data, to url: URL) throws {
		if self.fileExists(atPath: url.path) {
			let fileHandle = try FileHandle(forWritingTo: url)
			fileHandle.write(data)
			fileHandle.truncateFile(atOffset: fileHandle.offsetInFile)
			fileHandle.closeFile()
		} else {
			self.createFile(atPath: url.path,
			                contents: data,
			                attributes: nil)
		}
	}
}
