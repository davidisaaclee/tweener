import Foundation
import UIKit
import VectorSwift

struct ScrubPointSet {
	static var `default`: ScrubPointSet {
		return ScrubPointSet(startPath: CirclePath(center: CGPoint(x: -100, y: -200),
		                                           radius: 50),
		                     displacement: CGSize(width: 200, height: 400))
	}

	static func random(within bounds: CGSize, radius: CGFloat = 50) -> ScrubPointSet {
		let displacement =
			CGSize(width: CGFloat.random(within: 0 ... bounds.width) - bounds.width / 2.0,
			       height: CGFloat.random(within: 0 ... bounds.height) - bounds.height / 2.0)

		return ScrubPointSet(startPath: CirclePath(center: CGPoint(collection: -displacement),
		                                           radius: radius),
		                     displacement: displacement * 2)
	}

	var startPath: CirclePath
	var displacement: CGSize

	var endPath: UIBezierPath {
		let origin: CGPoint =
			self.startPath.center - CGPoint.unit * self.startPath.radius + self.displacement
		return UIBezierPath(ovalIn: CGRect(origin: origin,
		                                   size: CGSize.unit * self.startPath.radius * 2))
	}

	init(startPath: CirclePath, displacement: CGSize) {
		self.startPath = startPath
		self.displacement = displacement
	}
}


extension ScrubPointSet: Codable {
	init?(coder: NSCoder) {
		guard
			let startPath: CirclePath = coder.decode(forKey: "startPath")
			else {
				return nil
			}

		self.init(startPath: startPath,
		          displacement: coder.decodeCGSize(forKey: "displacement"))
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.startPath, forKey: "startPath")
		coder.encode(self.displacement, forKey: "displacement")
	}
}
