import Foundation

protocol HierarchicalNavigable {
	associatedtype Value

	static func make(with value: Value) -> Self
	@discardableResult func extend(with value: Value) -> Self
	func fork(with node: Self)
	@discardableResult func pop()
}

import UIKit

extension UINavigationController: HierarchicalNavigable {
	typealias Value = UIViewController

	static func make(with value: UIViewController) -> Self {
		return self.init(rootViewController: value)
	}

	@discardableResult func extend(with value: UIViewController) -> Self {
		self.pushViewController(value, animated: true)
		return self
	}

	func fork(with node: UINavigationController) {
		self.present(node, animated: false, completion: nil)
	}

	@discardableResult func pop() {
		if self.presentedViewController != nil {
			self.dismiss(animated: false, completion: nil)
		} else if self.viewControllers.count == 1 {
			self.dismiss(animated: false, completion: nil)
		} else {
			self.popViewController(animated: false)
		}
	}
}
