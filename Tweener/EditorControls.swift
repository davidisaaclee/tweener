import Foundation
import UIKit

typealias EditorControls =
	ViewControllerAttachable & WorkspaceUpdatable & ClosureBackedSceneConvertible

protocol ViewControllerAttachable {
	func attach(to parent: UIViewController, parentView: UIView?)
	func detachFromParent()
}

extension ViewControllerAttachable where Self: UIViewController {
	func attach(to parent: UIViewController, parentView: UIView? = nil) {
		let parentView: UIView = parentView ?? parent.view

		parent.addChildViewController(self)
		self.view.frame = parentView.bounds
		self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		parentView.addSubview(self.view)
		self.didMove(toParentViewController: parent)
	}

	func detachFromParent() {
		self.removeFromParentViewController()
		self.view.removeFromSuperview()
		self.didMove(toParentViewController: nil)
	}
}

protocol WorkspaceUpdatable {
	func update(with workspace: Workspace)
}

protocol SceneConvertible {
	func convert(_ point: CGPoint, toSceneFromView view: UIView) -> CGPoint
	func convert(_ point: CGPoint, fromSceneToView view: UIView) -> CGPoint
}

protocol ClosureBackedSceneConvertible: class, SceneConvertible {
	var convertPointToSceneFromView: ((CGPoint, UIView) -> CGPoint)? { get set }
	var convertPointFromSceneToView: ((CGPoint, UIView) -> CGPoint)? { get set }
}

extension SceneConvertible where Self: ClosureBackedSceneConvertible {
	func convert(_ point: CGPoint, toSceneFromView view: UIView) -> CGPoint {
		return self.convertPointToSceneFromView?(point, view) ?? point
	}

	func convert(_ point: CGPoint, fromSceneToView view: UIView) -> CGPoint {
		return self.convertPointFromSceneToView?(point, view) ?? point
	}
}
