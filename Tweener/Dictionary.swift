import Foundation

extension Dictionary {
	func withTransformedValue(at key: Key,
	                          transform: (Value) -> Value) -> Dictionary {
		var copy = self
		copy[key] = self[key].map(transform)
		return copy
	}
}

extension Dictionary {
	/// Creates a copy of `dictionary` which includes the provided key-value pair.
	static func byAppending<K, V>(_ keyValuePair: (K, V), to dictionary: Dictionary<K, V>) -> Dictionary<K, V> {
		var dictionaryʹ = dictionary
		dictionaryʹ[keyValuePair.0] = keyValuePair.1
		return dictionaryʹ
	}

	/// Creates a copy of this dictionary which includes the provided key-value pair.
	func appending(_ keyValuePair: (Key, Value)) -> Dictionary {
		return Dictionary.byAppending(keyValuePair, to: self)
	}
}

extension Dictionary {
	func mapValues<T>(_ transform: @escaping (Key, Value) -> T) -> [Key: T] {
		return self.reduce([:]) { (acc: [Key: T], keyValuePair: (key: Key, value: Value)) -> [Key: T] in
			let (key, value) = keyValuePair
			return acc.appending((key, transform(key, value)))
		}
	}
}

extension Dictionary {
	func merged(with otherDictionary: [Key: Value]) -> [Key: Value] {
		return otherDictionary.reduce(self, { $0.appending($1) })
	}
}
