import Foundation
import SpriteKit
import VectorSwift

protocol CanvasViewDelegate: class {
	// These methods communicate touch locations in the scene's coordinate space.
	func canvasView(_ canvasView: CanvasView,
	                didReceiveTouchBegan touch: UITouch,
	                at position: CGPoint)

	func canvasView(_ canvasView: CanvasView,
	                didReceiveTouchMoved touch: UITouch,
	                at position: CGPoint)

	func canvasView(_ canvasView: CanvasView,
	                didReceiveTouchMoved touch: UITouch,
	                by amount: CGPoint)

	func canvasView(_ canvasView: CanvasView,
	                didReceiveTouchEnded touch: UITouch,
	                at position: CGPoint)
}

class CanvasView: SKView {
	weak var canvasDelegate: CanvasViewDelegate?

	private var touchToLastLocation: [UITouch: CGPoint] = [:]

	let canvasScene = CanvasScene()

	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setup()
	}

	override func layoutSubviews() {
		super.layoutSubviews()

		self.canvasScene.size = self.bounds.size
	}

	fileprivate func setup() {
		// Set the scale mode to scale to fit the window
		canvasScene.scaleMode = .aspectFill

		// Present the scene
		self.presentScene(canvasScene)

		self.ignoresSiblingOrder = true
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesBegan(touches, with: event)

		for touch in touches {
			let location = touch.location(in: self.canvasScene)
			self.canvasDelegate?.canvasView(self,
			                                didReceiveTouchBegan: touch,
			                                at: location)
			self.touchToLastLocation[touch] = location
		}
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesMoved(touches, with: event)

		for touch in touches {
			let location = touch.location(in: self.canvasScene)
			self.canvasDelegate?.canvasView(self,
			                                didReceiveTouchMoved: touch,
			                                at: location)

			if let lastLocation = self.touchToLastLocation[touch] {
				self.canvasDelegate?.canvasView(self,
				                                didReceiveTouchMoved: touch,
				                                by: location - lastLocation)
			}

			self.touchToLastLocation[touch] = location
		}
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesEnded(touches, with: event)

		for touch in touches {
			self.canvasDelegate?.canvasView(self,
			                                didReceiveTouchEnded: touch,
			                                at: touch.location(in: self.canvasScene))
			self.touchToLastLocation[touch] = nil
		}
	}

	func convert(_ point: CGPoint, toSceneFromView view: UIView) -> CGPoint {
		return self.canvasScene.convertPoint(fromView: self.convert(point, from: view))
	}

	func convert(_ point: CGPoint, fromSceneToView view: UIView) -> CGPoint {
		return self.convert(self.canvasScene.convertPoint(toView: point),
		                    to: view)
	}
}
