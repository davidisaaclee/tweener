import UIKit

protocol ProjectManagerDelegate: class {
	func projectManager(_ projectManager: ProjectManagerViewController,
	                    didSelect project: Project,
	                    workspace: Workspace)

	func projectManagerDidRequestNewProject(_ projectManager: ProjectManagerViewController)
	func projectManager(_ projectManager: ProjectManagerViewController,
	                    didRequestDelete project: Project)
}


class ProjectManagerViewController: UIViewController {

	weak var delegate: ProjectManagerDelegate?

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.reloadData()
	}

//	fileprivate var data: [URL] = [] {
//		didSet {
//			self.tableView.reloadData()
//		}
//	}

	fileprivate var projectListing: ProjectListing? {
		didSet {
			self.tableView.reloadData()
		}
	}

	fileprivate var sortedProjects: [Project]? {
		return self.projectListing?.projects
			.values
			.sorted(by: { $0.dateCreated > $1.dateCreated })
	}


	@IBOutlet weak var tableView: UITableView! {
		didSet {
			self.tableView.delegate = self
			self.tableView.dataSource = self
		}
	}

	@IBAction func makeNewProject() {
		self.delegate?.projectManagerDidRequestNewProject(self)
	}

	func reloadData() {
		ProjectDatabase.shared.getProjectListing()
			.onSuccess { self.projectListing = $0 }
	}

}

extension ProjectManagerViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.projectListing?.projects.count ?? 0
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell =
			tableView.dequeueReusableCell(withIdentifier: "project") ??
				ProjectCell(style: .value1, reuseIdentifier: "project")

		cell.selectionStyle = .blue
		cell.textLabel?.text = self.sortedProjects![indexPath.row].name
		return cell
	}
}

extension ProjectManagerViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let project = self.sortedProjects![indexPath.row]
		ProjectDatabase.shared.loadWorkspace(for: project)
			.onSuccess { (workspace) in
				self.delegate?.projectManager(self,
				                              didSelect: project,
				                              workspace: workspace)
			}
			.onFailure {
				print("Failed to load workspace: \($0.localizedDescription)")
			}
	}

	func tableView(_ tableView: UITableView,
	               editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
		return .delete
	}

	func tableView(_ tableView: UITableView,
	               commit editingStyle: UITableViewCellEditingStyle,
	               forRowAt indexPath: IndexPath) {
		switch editingStyle {
		case .delete:
			self.delegate?.projectManager(self,
			                              didRequestDelete: self.sortedProjects![indexPath.row])

		default:
			// not supported
			break
		}
	}
}



class ProjectCell: UITableViewCell {
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)

		UIView.animateKeyframes(withDuration: animated ? 0.3 : 0,
		                        delay: 0,
		                        options: [],
		                        animations: {
															self.backgroundColor =
																selected
																? UIColor.white.withAlphaComponent(0.3)
																: .clear
		}, completion: nil)
	}
}
