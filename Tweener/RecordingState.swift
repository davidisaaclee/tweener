import Foundation

struct RecordingState {
	let fps: Double

	var frameIndex: Int {
		return Int(self.fractionalFrameIndex)
	}

	fileprivate var lastUpdated: Date = Date()
	fileprivate var fractionalFrameIndex: Float = 0

	init(fps: Double = 60) {
		self.fps = fps
	}

	func updated(with time: Date) -> RecordingState {
		var copy = self
		copy.fractionalFrameIndex +=
			Float(time.timeIntervalSince(self.lastUpdated) * self.fps)
		copy.lastUpdated = time
		return copy
	}
}


extension RecordingState: Codable {
	init?(coder: NSCoder) {
		// Not worrying about decoding recording progress.
		self.init(fps: coder.decodeDouble(forKey: "fps"))
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.fps, forKey: "fps")
	}
}


extension RecordingState: Equatable {
	static func == (lhs: RecordingState, rhs: RecordingState) -> Bool {
		return lhs.fps == rhs.fps
			&& lhs.lastUpdated == rhs.lastUpdated
			&& lhs.fractionalFrameIndex == rhs.fractionalFrameIndex
	}
}