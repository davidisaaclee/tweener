import Foundation
import UIKit
import VectorSwift

class CoverAnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning {
	enum Direction {
		case top
		case bottom
		case left
		case right
	}

	var direction: Direction = .bottom
	var isPresenting: Bool = false

	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		guard
			let fromViewController = transitionContext.viewController(forKey: .from),
			let toViewController = transitionContext.viewController(forKey: .to)
			else {
				return
			}

		let containerView = transitionContext.containerView

		if isPresenting {
			containerView.addSubview(fromViewController.view)

			toViewController.view.frame = containerView.bounds
			containerView.addSubview(toViewController.view)

			var offsetFactor: CGPoint {
				switch self.direction {
				case .top:
					return CGPoint(x: 0, y: -1)
				case .bottom:
					return CGPoint(x: 0, y: 1)
				case .left:
					return CGPoint(x: -1, y: 0)
				case .right:
					return CGPoint(x: 1, y: 0)
				}
			}

			toViewController.view.center =
				toViewController.view.center + containerView.bounds.size * offsetFactor

			func animations() {
				toViewController.view.center =
					containerView.bounds.origin + containerView.bounds.size * 0.5
			}

			func completion(completed: Bool) {
				let completed = !transitionContext.transitionWasCancelled && completed

				if !completed {
					containerView.addSubview(fromViewController.view)
					toViewController.view.removeFromSuperview()
				}

				transitionContext.completeTransition(completed)
			}

			UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
			               delay: 0,
			               options: [],
			               animations: animations,
			               completion: completion(completed:))
		} else {

			toViewController.view.frame = containerView.bounds
			containerView.addSubview(toViewController.view)

			containerView.insertSubview(fromViewController.view,
			                            aboveSubview: toViewController.view)

			var offsetFactor: CGPoint {
				switch self.direction {
				case .top:
					return CGPoint(x: 0, y: -1)
				case .bottom:
					return CGPoint(x: 0, y: 1)
				case .left:
					return CGPoint(x: -1, y: 0)
				case .right:
					return CGPoint(x: 1, y: 0)
				}
			}

			let offset: CGPoint = containerView.bounds.size * offsetFactor

			func animations() {
				fromViewController.view.center =
					fromViewController.view.center + offset
			}

			func completion(completed: Bool) {
				let completed = !transitionContext.transitionWasCancelled && completed

//				if !completed {
//					containerView.addSubview(fromViewController.view)
//					toViewController.view.removeFromSuperview()
//				}

				transitionContext.completeTransition(completed)
			}

			UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
			               delay: 0,
			               options: [],
			               animations: animations,
			               completion: completion(completed:))

		}
	}

	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return 0.3
	}
}
