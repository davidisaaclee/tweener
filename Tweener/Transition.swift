import Foundation

struct Transition {
	typealias ID = UUID

	let id: ID
	let fromKeyID: Key.ID
	var toKeyID: Key.ID?

	var animation: Animation
	var interaction: Interaction

	init(id: ID = ID(),
	     fromKeyID: Key.ID,
	     toKeyID: Key.ID? = nil,
	     animation: Animation,
	     interaction: Interaction) {
		self.id = id
		self.fromKeyID = fromKeyID
		self.toKeyID = toKeyID
		self.animation = animation
		self.interaction = interaction
	}
}


extension Transition: Codable {
	init?(coder: NSCoder) {
		guard
			let id: UUID = coder.decode(forKey: "id"),
			let fromKeyID: Key.ID = coder.decode(forKey: "fromKeyID"),
			let toKeyID: UUID? = coder.decodeOptional(forKey: "toKeyID"),
			let animation: Animation = coder.decode(forKey: "animation"),
			let interaction: Interaction = coder.decode(forKey: "interaction")
			else {
				return nil
			}

		self.init(id: id,
		          fromKeyID: fromKeyID,
		          toKeyID: toKeyID,
		          animation: animation,
		          interaction: interaction)
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.id, forKey: "id")
		coder.encode(self.fromKeyID, forKey: "fromKeyID")
		coder.encode(optional: self.toKeyID, forKey: "toKeyID")
		coder.encode(self.animation, forKey: "animation")
		coder.encode(self.interaction, forKey: "interaction")
	}
}
