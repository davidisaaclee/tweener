import Foundation
import CoreGraphics
import VectorSwift

struct Animation {
	typealias ID = UUID

	struct Frame {
		var diffs: [Object.ID: Object.Diff]

		static func tween(between startFrame: Frame,
		                  and endFrame: Frame,
		                  amount: Float) -> Frame {
			let resultDiffs = startFrame.diffs.reduce(endFrame.diffs) { (dict, kvPair) in
				guard let endDiff = dict[kvPair.key] else {
					return dict
				}
				let startDiff = kvPair.value

				return dict.appending((kvPair.key, Object.Diff.tween(from: startDiff,
				                                                     to: endDiff,
				                                                     amount: amount)))
			}

			return Frame(diffs: resultDiffs)
		}
	}

	struct Interpolation {
		let startFrame: Frame
		let endFrame: Frame
		
		/**
		A value between 0 and 1 indicating the interpolation weight between the
		two frames.

		- 0 indicates that only `startFrame` should be used.
		- 1 indicates that only `endFrame` should be used.
		*/
		let progress: Float
	}

	let id: ID

	private(set) var frames: [Frame]

	init(id: ID = ID(), frames: [Frame]) {
		self.id = id
		self.frames = frames
	}


	// MARK: Accessing frames

	func frameIndex(atPosition position: Float) -> Int {
		guard (0.0 ..< 1.0).contains(position) else {
			fatalError()
		}

		return Int(floor(position * Float(self.frames.count)))
	}

	func frame(atPosition position: Float) -> Frame {
		return self.frames[self.frameIndex(atPosition: position)]
	}


	// MARK: Indexing

	func incrementLoopingFrameIndex(_ index: Int) -> Int {
		return (index + 1) % self.frames.count
	}

	/**
	The number of frames it takes to get from `startFrameIndex` and
	`endFrameIndex` in a forward-moving loop of this animation.
	
	```
	animation.frames.count == 4

	animation.numberOfLoopingFrames(from: 0, to: 1) == 1
	animation.numberOfLoopingFrames(from: 2, to: 0) == 2
	```
	*/
	func numberOfLoopingFrames(from startFrameIndex: Int,
	                           to endFrameIndex: Int) -> Int {
		return ((endFrameIndex + self.frames.count) - startFrameIndex) % self.frames.count
	}


	// MARK: Inserting frames

	mutating func insert(_ frame: Animation.Frame, at position: Float) {
		self.insert(frame, at: self.frameIndex(atPosition: position))
	}

	mutating func insert(_ frame: Animation.Frame, at frameIndex: Int) {
		// Pad recording with empty frames until the recording has enough frames
		// to insert the new frame at `frameIndex`.
		while frameIndex >= self.frames.count {
			let emptyFrame = Frame(diffs: [:])
			self.frames.append(emptyFrame)
		}

		// Merge the new framew with the one that's there already.
		self.frames[frameIndex].diffs =
			frame.diffs
				.reduce(self.frames[frameIndex].diffs) { $0.appending($1) }
	}


	// MARK: Mutation

	mutating func remove(objectWithID objectID: Object.ID) {
		self.frames = self.frames.map { (frame) -> Animation.Frame in
			var frameʹ = frame
			frameʹ.diffs.removeValue(forKey: objectID)
			return frameʹ
		}
	}

	/**
	Returns a copy of this animation, with added or removed frames to match the 
	timescale.
	
	a.timescaled(byFactorOf: 0) == a @ 1x speed 
	a.timescaled(byFactorOf: 1) == a @ 2x speed
	a.timescaled(byFactorOf: 0.5) == a @ 0.5x speed
	*/
	func timescaled(byFactorOf factor: Int) -> Animation {
		guard factor != 0 else {
			return self
		}

		var resultFrames: [Animation.Frame]
		if factor > 0 {
			resultFrames = self.frames
				.enumerated()
				.filter { $0.offset % 2 == 0 }
				.map { $0.element }
			return Animation(frames: resultFrames).timescaled(byFactorOf: factor - 1)
		} else {
			resultFrames = self.frames.enumerated().flatMap { (offset, frame) -> [Animation.Frame] in
				if self.frames.indices.contains(offset + 1) {
					let startFrame = frame
					let endFrame = self.frames[offset + 1]

					let interpolatedDiffs: [Object.ID: Object.Diff] =
						endFrame.diffs.values.reduce(startFrame.diffs) { (diffs, endDiff) in
							var diffsʹ = diffs

							if let startDiff = diffs[endDiff.objectID] {
								diffsʹ[endDiff.objectID] =
									Object.Diff(objectID: endDiff.objectID,
									            position: startDiff.position + (endDiff.position - startDiff.position) * 0.5,
									            isHidden: endDiff.isHidden)
							} else {
								// TODO: Interpolate from key
								diffsʹ[endDiff.objectID] = endDiff
							}

							return diffsʹ
						}
					return [frame, Animation.Frame(diffs: interpolatedDiffs)]
				}
				
				return [frame]
			}

			return Animation(frames: resultFrames).timescaled(byFactorOf: factor + 1)
		}
	}
}


extension Animation: Codable {
	init?(coder: NSCoder) {
		guard
			let id: UUID = coder.decode(forKey: "id"),
			let frames: [Animation.Frame] = coder.decodeArray(forKey: "frames")
			else {
				return nil
			}

		self.init(id: id, frames: frames)
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.id, forKey: "id")
		coder.encode(self.frames, forKey: "frames")
	}
}


extension Animation.Frame: Codable {
	init?(coder: NSCoder) {
		guard let objectDiffs: [Object.Diff] = coder.decodeArray(forKey: "objectDiffs") else {
			return nil
		}

		let objectDiffDictionary = objectDiffs
			.map { ($0.objectID, $0) }
			.reduce([:]) { $0.appending($1) }

		self.init(diffs: objectDiffDictionary)
	}

	func encode(with coder: NSCoder) {
		coder.encode(Array(self.diffs.values), forKey: "objectDiffs")
	}
}

