import UIKit
import VectorSwift
import AudioToolbox

class CanvasEditorViewController: UIViewController {
	let canvasView = CanvasView()
	let controlsView = UIView()

	private let modeBorderWidth: CGFloat = 20
	private let modeBorderView = UIView()
	var modeBorderColor: UIColor = .clear {
		didSet {
			self.modeBorderView.backgroundColor = modeBorderColor
		}
	}

	var progress: CGFloat = 0 {
		willSet {
			guard let workspace = self.workspace else {
				return
			}

			guard workspace.isClickEnabled else {
				return
			}

			if case .scrub = workspace.mode {
				return
			}

			guard let playback = workspace.playback else {
				return
			}

			guard let animation = workspace.currentAnimation else {
				return
			}

			let loopDuration =
				animation.duration(whenPlayedWith: playback) * 60 / playback.fps
			let minBeatDuration = 0.8


			guard loopDuration > minBeatDuration else {
				return
			}

			var harmonic: Double = 0
			while !(minBeatDuration ... minBeatDuration * 2).contains(loopDuration / pow(2.0, harmonic)) {
				harmonic += 1
			}

			let frequency = Int(pow(2.0, harmonic))

			(0..<frequency)
				.map { CGFloat($0) / CGFloat(frequency) }
				.filter { self.progress < $0 && newValue > $0 }
				.first
				.tap { _ in AudioServicesPlaySystemSound(1519) }

			// on jump from 1 to 0
			if self.progress > 0.5 && newValue < 0.5 {
				AudioServicesPlaySystemSound(1520)
			}
		}

		didSet {
			let indicatorCenter =
				CGPoint(x: self.modeBorderView.bounds.width * self.progress,
				        y: self.modeBorderView.bounds.midY)


			let radius: CGFloat = 2
			let cornerRadius: CGFloat = 2
			let overhang: CGFloat = 10

			let rect =
				CGRect(origin: indicatorCenter - CGSize(width: radius, height: self.modeBorderView.bounds.height / 2 + cornerRadius),
				       size: CGSize(width: radius * 2, height: self.modeBorderView.bounds.height + overhang + cornerRadius))
			let path = UIBezierPath(roundedRect: rect, cornerRadius: 2)

			self.progressLayer.path = path.cgPath

			let progressColor: UIColor
			switch self.workspace?.mode {
			case .some(.recording), .some(.overdub):
				progressColor = .white
			default:
				progressColor = .appRed
			}


			self.progressLayer.fillColor = progressColor.cgColor
		}
	}

	// TODO: Make private again
	var activeControls: EditorControls?

	private let progressLayer = CAShapeLayer()

	private var previousWorkspace: Workspace?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.progressLayer.frame = self.modeBorderView.layer.bounds
		self.modeBorderView.layer.addSublayer(self.progressLayer)

		self.canvasView.frame = self.view.bounds
		self.canvasView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		self.view.addSubview(self.canvasView)

		self.controlsView.frame = self.view.bounds
		self.controlsView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		self.view.addSubview(self.controlsView)

		self.view.addSubview(self.modeBorderView)
		self.modeBorderView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint
			.activate([self.modeBorderView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
			           self.modeBorderView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
			           self.modeBorderView.topAnchor.constraint(equalTo: self.view.topAnchor),
			           self.modeBorderView.heightAnchor.constraint(equalToConstant: self.modeBorderWidth)])
	}

	func update(with workspace: Workspace) {
		self.previousWorkspace = workspace
		self.activeControls?.update(with: workspace)
		self.updateCanvas(using: workspace)
		self.updateAnimationProgress(using: workspace)
		self.updateModeBorder(using: workspace)
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		self.progressLayer.frame = self.canvasView.layer.bounds
	}

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	func activate(_ controls: EditorControls) {
		self.deactivateControls()

		controls.attach(to: self, parentView: self.controlsView)

		self.activeControls = controls

		controls.convertPointToSceneFromView =
			self.canvasView.convert(_:toSceneFromView:)
		controls.convertPointFromSceneToView =
			self.canvasView.convert(_:fromSceneToView:)

		if let previousWorkspace = self.previousWorkspace {
			controls.update(with: previousWorkspace)
		}
	}

	func deactivateControls() {
		guard let activeControls = self.activeControls else {
			return
		}
		activeControls.detachFromParent()
	}

	fileprivate var workspace: Workspace?

	private func updateCanvas(using workspace: Workspace) {
		if case .moveScrubPoint = workspace.mode {
			return
		}

		self.workspace = workspace
		self.canvasView.canvasScene.workspace = workspace
		self.canvasView.setNeedsDisplay()
	}

	private func updateAnimationProgress(using workspace: Workspace) {
		guard !(.layout ~= workspace.mode) else {
			self.progress = 0
			return
		}

		if let playback = workspace.playback {
			self.progress = CGFloat(playback.position)
		}
	}


	private func updateModeBorder(using workspace: Workspace) {
		switch workspace.mode {
		case .layout:
			self.modeBorderColor = .appCyan

		case .playback:
			self.modeBorderColor = .appBlack

		case .recording:
			self.modeBorderColor = .appRed

		case .overdub:
			self.modeBorderColor = .appRed

		case .scrub, .tapped, .moveScrubPoint, .moveTapPoint, .interact:
			self.modeBorderColor = .appBlue

		case .editInteractions:
			self.modeBorderColor = .appWhite
		}
	}
}
