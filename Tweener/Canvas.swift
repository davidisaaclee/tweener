import Foundation
import UIKit

struct Canvas {
	var objects: [Object.ID: Object]
}

extension Canvas: Codable {
	init?(coder: NSCoder) {
		guard let objects: [Object] = coder.decodeArray(forKey: "objects") else {
			return nil
		}

		let objectDictionary: [Object.ID: Object] = objects
			.map { ($0.id, $0) }
			.reduce([:]) { $0.appending($1) }

		self.init(objects: objectDictionary)
	}

	func encode(with coder: NSCoder) {
		coder.encode(Array(self.objects.values), forKey: "objects")
	}
}
