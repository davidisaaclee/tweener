import Foundation
import SpriteKit

class CanvasScene: SKScene {
	private var objects: Set<Object> {
		guard let workspace = self.workspace else {
			return []
		}

		return Set(workspace.canvasBuffer.objects.values)
	}

	var workspace: Workspace?

	let cameraNode = SKCameraNode()

	fileprivate var nodes: [UUID: SKNode] = [:]

	override func didMove(to view: SKView) {
		self.addChild(self.cameraNode)
		self.camera = self.cameraNode

		self.backgroundColor = .appLightRed
	}

	override func update(_ currentTime: TimeInterval) {
		self.objects.forEach { (object) in
			switch object.content {
			case let .text(string, font):
				if self.nodes[object.id] == nil {
					let node = SKLabelNode()
					node.text = string
					node.fontName = font.fontName
					node.fontSize = font.pointSize

					self.addChild(node)
					self.nodes[object.id] = node
				}

				guard let node = self.nodes[object.id] else {
					fatalError()
				}

				node.position = object.position
			}
		}

		let idSet = Set(self.nodes.keys)
		let idsOfNodesToRemove = idSet.subtracting(self.objects.map { $0.id })
		idsOfNodesToRemove.forEach {
			self.nodes[$0]?.removeFromParent()
			self.nodes.removeValue(forKey: $0)
		}
	}
}
