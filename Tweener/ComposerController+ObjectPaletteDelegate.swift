import Foundation

extension ComposerController: ObjectPaletteDelegate {
	func objectPaletteDidTapHomeButton(_ objectPalette: ObjectPaletteViewController) {
		self.navigationController.pop()
	}

	func objectPalette(_ objectPalette: ObjectPaletteViewController,
	                   didChooseObject object: Object) {
		self.workspace.saveToHistory()

		self.workspace.insertObjectIntoCurrentKeyframe(object)
		self.workspace.mode = .layout
		self.workspace.playback = nil
		self.navigationController.pop()
	}
}
