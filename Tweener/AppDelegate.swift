import UIKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	var controller: ComposerController!


	func application(_ application: UIApplication,
	                 didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		BuddyBuildSDK.setup()


		try! AVAudioSession.sharedInstance()
			.setCategory(AVAudioSessionCategoryAmbient, with: .mixWithOthers)

		let navigationController = NavigationController()
		self.controller =
			ComposerController(navigationController: navigationController,
			                   canvasEditor: CanvasEditorViewController())


		navigationController.controller = self.controller

		self.controller.navigationController
			.pushViewController(self.controller.makeProjectManager(), animated: false)

		self.window = UIWindow()
		self.window?.rootViewController = self.controller.navigationController
		self.window?.makeKeyAndVisible()

		let layoutEditor = self.controller.makeLayoutEditor()
		self.controller.canvasEditor.activate(layoutEditor)
		self.controller.canvasEditor.update(with: self.controller.workspace)

		return true
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
	}

	func applicationWillResignActive(_ application: UIApplication) {
		guard self.controller.project != nil else {
			return
		}

		// Save project on exit.
		ProjectDatabase.shared.save(self.controller.workspace, as: self.controller.project)
			.onFailure {
				print("Failed to save project: \($0.localizedDescription)")
			}
	}

}

fileprivate class NavigationController: UINavigationController {
	weak var controller: ComposerController?

	fileprivate override func viewDidLoad() {
		super.viewDidLoad()

		self.isNavigationBarHidden = true
	}
}
