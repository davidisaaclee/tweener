import UIKit

extension UIBezierPath {
	convenience init(circleWithRadius radius: CGFloat, center: CGPoint = .zero) {
		self.init(ovalIn: CGRect(x: -radius + center.x, y: -radius + center.y,
		                         width: radius * 2, height: radius * 2))
	}
}
