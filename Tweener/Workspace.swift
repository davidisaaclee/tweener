import Foundation
import UIKit
import VectorSwift

struct Workspace {

	enum EditMode {
		case layout
		case playback
		case recording(state: RecordingState)
		case overdub
		case interact
		case scrub(activeTransitionID: Transition.ID)
		case tapped(activeTransitionID: Transition.ID, startDate: Date)
		case moveScrubPoint(interactionID: Interaction.ID, isMovingStartPoint: Bool)
		case moveTapPoint(interactionID: Interaction.ID)
		case editInteractions
	}

	typealias ID = UUID

	let id: ID

	// MARK: Document

	var allKeys: [Key.ID: Key] {
		didSet {
			self.updateObservers()
		}
	}

	var allTransitions: [Transition.ID: Transition] {
		didSet {
			self.updateObservers()
		}
	}


	// MARK: State

	/// The canvas which will be rendered, and from which a recording is captured.
	var canvasBuffer: Canvas {
		didSet {
			self.updateObservers()
		}
	}

	var currentKeyID: Key.ID {
		didSet {
			self.updateObservers()
		}
	}

	var currentKey: Key {
		return self.allKeys[self.currentKeyID]!
	}

	var currentTransitionID: Transition.ID {
		didSet {
			self.updateObservers()
		}
	}

	var currentTransition: Transition? {
		return self.allTransitions[self.currentTransitionID]
	}

	var currentAnimation: Animation? {
		return self.currentTransition?.animation
	}

	var currentInteraction: Interaction? {
		return self.currentTransition?.interaction
	}

	var mode: EditMode = .layout {
		didSet {
			self.updateObservers()
		}
	}

	var playback: Playback?
	var touchedObjectIDs: Set<UUID>

	var history: [Workspace] = []

	// MARK: Configuration

	var isClickEnabled: Bool = false

	init(id: ID,
	     allKeys: [Key.ID: Key],
	     allTransitions: [Transition.ID: Transition],
	     canvasBuffer: Canvas,
	     currentKeyID: Key.ID,
	     currentTransitionID: Transition.ID,
	     mode: EditMode,
	     playback: Playback?,
	     touchedObjectIDs: Set<UUID>,
	     history: [Workspace],
	     isClickEnabled: Bool) {
		self.id = id
		self.allKeys = allKeys
		self.allTransitions = allTransitions
		self.canvasBuffer = canvasBuffer
		self.currentKeyID = currentKeyID
		self.currentTransitionID = currentTransitionID
		self.mode = mode
		self.playback = playback
		self.touchedObjectIDs = touchedObjectIDs
		self.history = history
		self.isClickEnabled = isClickEnabled
	}


	init(canvas: Canvas = Canvas(objects: [:])) {
		self.id = ID()
		self.touchedObjectIDs = []

		self.allKeys = [:]
		self.allTransitions = [:]
		self.canvasBuffer = canvas

		let key = Key(keyframe: canvas)
		// this is kind of annoying that i need to set this
		self.currentKeyID = key.id

		// HACK: need to set this before using self
		self.currentTransitionID = UUID()

		let transition = self.addTransition(from: key.id)
		self.currentTransitionID = transition.id

		self.insert(key)
	}


	// MARK: Private

	var observers: [UUID: (Workspace) -> Void] = [:]

	fileprivate func updateObservers() {
		self.observers.values.forEach { $0(self) }
	}
}


// MARK: - Convenience methods

extension Workspace {

	func transitions(for key: Key) -> [Transition] {
		return self.allTransitions
			.values
			.filter { $0.fromKeyID == key.id }
	}

	mutating func insert(_ key: Key) {
		self.allKeys[key.id] = key
	}

	@discardableResult mutating func addTransition(from keyID: Key.ID) -> Transition {
		let interaction: Interaction =
//			.scrub(Interaction.ID(),
//			       .random(within: CGSize(width: 300, height: 600)))
			.tap(id: Interaction.ID(),
			     bounds: CirclePath.init(center: .zero, radius: 80))
		let newTransition = Transition(fromKeyID: keyID,
		                               toKeyID: nil,
		                               animation: Animation(frames: []),
		                               interaction: interaction)
		self.allTransitions[newTransition.id] = newTransition
		return newTransition
	}

	mutating func insertObjectIntoCurrentKeyframe(_ object: Object) {
		self = Key.currentKeyFromWorkspace
			.composed(with: Object.objectFromKeyframe(forID: object.id))
			.set(object)
			<| self
	}

	mutating func modifyObjectInCurrentKeyframe(forID objectID: Object.ID,
	                                            transform: @escaping (Object?) -> Object?) {
		self = Key.currentKeyFromWorkspace
			.composed(with: Object.objectFromKeyframe(forID: objectID))
			.over(transform)
			<| self
	}

	@discardableResult mutating func removeObjectFromKey(objectID: Object.ID) -> Object? {
		let objectToDelete = self.currentKey.keyframe.objects[objectID]

		self = Key.currentKeyFromWorkspace.over { (key) -> Key in
			var keyʹ = key
			keyʹ.keyframe.objects.removeValue(forKey: objectID)
			return keyʹ
		}
		<| self

		self.allTransitions = self.transitions(for: self.currentKey)
			.map { (transition) -> Transition in
				var transitionʹ = transition
				transitionʹ.animation.remove(objectWithID: objectID)
				return transitionʹ
			}
			.reduce(self.allTransitions) { $0.appending(($1.id, $1)) }

		return objectToDelete
	}

	mutating func modifyCurrentAnimation(transform: (Animation) -> Animation) {
		self.allTransitions[self.currentTransitionID]!.animation =
			transform(self.allTransitions[self.currentTransitionID]!.animation)
	}

	mutating func modifyInteraction(withID interactionID: Interaction.ID,
	                                transform: (Interaction) -> Interaction) {
		self.allTransitions.values
			.filter { $0.interaction.id == interactionID }
			.first
			.map { $0.id }
			.tap { (transitionID) in
				self.allTransitions[transitionID]?.interaction =
					transform(self.allTransitions[transitionID]!.interaction)
			}
	}

	mutating func modifyCurrentInteraction(transform: (Interaction) -> Interaction) {
		self.allTransitions[self.currentTransitionID]!.interaction =
			transform(self.allTransitions[self.currentTransitionID]!.interaction)
	}

	/**
	Returns a copy of this workspace, deleting the specified transition and all
	descendants of that transition.
	*/
	func deletingTransition(withID transitionID: Transition.ID) -> Workspace {
		guard let transitionToDelete = self.allTransitions[transitionID] else {
			return self
		}

		var workspaceʹ = self
		workspaceʹ.allTransitions.removeValue(forKey: transitionID)

		if let toKeyID = transitionToDelete.toKeyID, let childKey = workspaceʹ.allKeys[toKeyID] {
			return workspaceʹ.transitions(for: childKey)
				.map { $0.id }
				.reduce(workspaceʹ) { $0.deletingTransition(withID: $1) }
		} else {
			return workspaceʹ
		}
	}

	func deletingKey(withID keyID: Key.ID) -> Workspace {
		guard self.allKeys.keys.contains(keyID) else {
			return self
		}

		var workspaceʹ = self

		// Remove the key.
		workspaceʹ.allKeys.removeValue(forKey: keyID)

		// Remove references from parent transitions.
		workspaceʹ.allTransitions =
			workspaceʹ.allTransitions
				.values
				.filter { $0.toKeyID == keyID }
				.reduce(workspaceʹ.allTransitions) { (transitions, parentTransition) in
					var transitionsʹ = transitions
					var parentTransitionʹ = parentTransition
					parentTransitionʹ.toKeyID = nil
					transitionsʹ[parentTransitionʹ.id] = parentTransitionʹ
					return transitionsʹ
				}

		// Remove all children.
		return workspaceʹ.allTransitions
			.values
			.filter { $0.fromKeyID == keyID }
			.reduce(workspaceʹ) { (workspace, childTransition) in
				var workspaceʹ = workspace
				workspaceʹ.allTransitions.removeValue(forKey: childTransition.id)
				if let childDestinationID = childTransition.toKeyID {
					return workspaceʹ.deletingKey(withID: childDestinationID)
				} else {
					return workspaceʹ
				}
			}
	}

	mutating func saveToHistory() {
		self.history.append(self)
	}

	mutating func undo() {
		guard let previousState = self.history.last else {
			return
		}

		self = previousState
	}

}


// MARK: - Observable

extension Workspace: Observable {
	mutating func addObserver(_ observer: @escaping (Workspace) -> Void) -> UUID {
		let id = UUID()
		self.observers[id] = observer
		return id
	}

	mutating func removeObserver(for token: UUID) {
		self.observers.removeValue(forKey: token)
	}
}


// MARK: - Codable

extension Workspace: Codable {
	init?(coder: NSCoder) {
		guard
			let id: Workspace.ID = coder.decode(forKey: "id"),
			let keysArray: [Key] = coder.decodeArray(forKey: "allKeys"),
			let transitionsArray: [Transition] = coder.decodeArray(forKey: "allTransitions"),
			let canvasBuffer: Canvas = coder.decode(forKey: "canvasBuffer"),
			let currentKeyID: Key.ID = coder.decode(forKey: "currentKeyID"),
			let currentTransitionID: Transition.ID = coder.decode(forKey: "currentTransitionID"),
			let mode: EditMode = coder.decode(forKey: "mode"),
			let playback: Playback? = coder.decodeOptional(forKey: "playback"),
			let touchedObjectIDsArray: [UUID] = coder.decodeArray(forKey: "touchedObjectIDs")
//			let history: [Workspace] = coder.decodeArray(forKey: "history")
			else {
				return nil
		}

		let isClickEnabled: Bool = coder.decodeBool(forKey: "isClickEnabled")

		let allKeys = keysArray
			.map { ($0.id, $0) }
			.reduce([:]) { $0.appending($1) }
		let allTransitions = transitionsArray
			.map { ($0.id, $0) }
			.reduce([:]) { $0.appending($1) }
		let touchedObjectIDs = Set<UUID>(touchedObjectIDsArray)

		self.init(id: id,
		          allKeys: allKeys,
		          allTransitions: allTransitions,
		          canvasBuffer: canvasBuffer,
		          currentKeyID: currentKeyID,
		          currentTransitionID: currentTransitionID,
		          mode: mode,
		          playback: playback,
		          touchedObjectIDs: touchedObjectIDs,
		          history: [],
		          isClickEnabled: isClickEnabled)
	}
	
	func encode(with coder: NSCoder) {
		coder.encode(self.id, forKey: "id")
		coder.encode(Array(self.allKeys.values), forKey: "allKeys")
		coder.encode(Array(self.allTransitions.values), forKey: "allTransitions")
		coder.encode(self.canvasBuffer, forKey: "canvasBuffer")
		coder.encode(self.currentKeyID, forKey: "currentKeyID")
		coder.encode(self.currentTransitionID, forKey: "currentTransitionID")
		coder.encode(self.mode, forKey: "mode")
		coder.encode(optional: self.playback, forKey: "playback")
		coder.encode(Array(self.touchedObjectIDs), forKey: "touchedObjectIDs")
//		coder.encode(self.history, forKey: "history")
		coder.encode(self.isClickEnabled, forKey: "isClickEnabled")
	}
}

extension Workspace.EditMode: Codable {
	init?(coder: NSCoder) {
		guard
			let associatedData = coder.decodeObject(forKey: "associatedData") as? Data,
			let typeTag = coder.decodeObject(forKey: "typeTag") as? String
			else {
				return nil
			}

		let subunarchiver = NSKeyedUnarchiver(forReadingWith: associatedData)

		switch typeTag {
		case "layout":
			self = .layout

		case "playback":
			self = .playback

		case "recording":
			guard let recordingState: RecordingState = subunarchiver.decode(forKey: "recordingState") else {
				return nil
			}
			self = .recording(state: recordingState)

		case "overdub":
			self = .overdub

		case "scrub":
			guard let activeTransitionID: Transition.ID = subunarchiver.decode(forKey: "activeTransitionID") else {
				return nil
			}
			self = .scrub(activeTransitionID: activeTransitionID)

		case "moveScrubPoint":
			guard let interactionID: Interaction.ID = subunarchiver.decode(forKey: "interactionID") else {
				return nil
			}
			self = .moveScrubPoint(interactionID: interactionID,
			                       isMovingStartPoint: subunarchiver.decodeBool(forKey: "isMovingStartPoint"))

		case "moveTapPoint":
			guard let interactionID: Interaction.ID = subunarchiver.decode(forKey: "interactionID") else {
				return nil
			}
			self = .moveTapPoint(interactionID: interactionID)

		case "editInteractions":
			self = .editInteractions

		case "interact":
			self = .interact

		default:
			return nil
		}

		subunarchiver.finishDecoding()
	}

	func encode(with coder: NSCoder) {
		var associatedData: Data {
			let data = NSMutableData()
			let subarchiver = NSKeyedArchiver(forWritingWith: data)

			switch self {
			case .layout, .playback, .overdub, .editInteractions, .interact:
				break

			case let .scrub(activeTransitionID):
				subarchiver.encode(activeTransitionID, forKey: "activeTransitionID")

			case let .tapped(activeTransitionID, startDate):
				subarchiver.encode(activeTransitionID, forKey: "activeTransitionID")
				subarchiver.encode(startDate, forKey: "startDate")

			case let .recording(state):
				subarchiver.encode(state, forKey: "recordingState")

			case let .moveScrubPoint(interactionID, isMovingStartPoint):
				subarchiver.encode(interactionID, forKey: "interactionID")
				subarchiver.encode(isMovingStartPoint, forKey: "isMovingStartPoint")

			case let .moveTapPoint(interactionID):
				subarchiver.encode(interactionID, forKey: "interactionID")
			}

			subarchiver.finishEncoding()
			return data as Data
		}

		var typeTag: String {
			switch self {
				case .layout:
					return "layout"

				case .playback:
					return "playback"

				case .recording:
					return "recording"

				case .overdub:
					return "overdub"

				case .scrub:
					return "scrub"

			case .tapped:
				return "tapped"

				case .moveScrubPoint:
					return "moveScrubPoint"

			case .moveTapPoint:
				return "moveTapPoint"

				case .editInteractions:
					return "editInteractions"

			case .interact:
				return "interact"
			}
		}

		coder.encode(typeTag, forKey: "typeTag")
		coder.encode(associatedData, forKey: "associatedData")
	}
}

extension Workspace.EditMode: Equatable {
	static func == (lhs: Workspace.EditMode, rhs: Workspace.EditMode) -> Bool {
		switch (lhs, rhs) {
		case (.layout, .layout):
			return true

		case (.playback, .playback):
			return true

		case (.overdub, .overdub):
			return true

		case (.interact, .interact):
			return true

		case (.editInteractions, .editInteractions):
			return true

		case let (.recording(leftState), .recording(rightState)):
			return leftState == rightState

		case let (.scrub(leftActiveTransitionID), .scrub(rightActiveTransitionID)):
			return leftActiveTransitionID == rightActiveTransitionID

		case let (.tapped(leftActiveTransitionID, leftStartDate), .tapped(rightActiveTransitionID, rightStartDate)):
			return leftActiveTransitionID == rightActiveTransitionID && leftStartDate == rightStartDate

		case let (.moveScrubPoint(leftInteractionID, leftIsMovingStartPoint), .moveScrubPoint(rightInteractionID, rightIsMovingStartPoint)):
			return (leftInteractionID == rightInteractionID) && (leftIsMovingStartPoint == rightIsMovingStartPoint)

		case let (.moveTapPoint(leftInteractionID), .moveTapPoint(rightInteractionID)):
			return (leftInteractionID == rightInteractionID)

		default: 
			return false
		}
	}
}


// MARK: - Lenses

fileprivate extension Key {
	static var currentKeyFromWorkspace: Lens<Workspace, Key> {
		return Lens<Workspace, Key>(set: { (key, workspace) -> Workspace in
			var workspaceʹ = workspace
			workspaceʹ.allKeys[workspace.currentKeyID] = key
			return workspaceʹ
		}, view: { (workspace) -> Key in
			return workspace.allKeys[workspace.currentKeyID]!
		})
	}

	static var keyframeObjects: Lens<Key, [Object.ID: Object]> {
		return Lens<Key, [Object.ID: Object]>(set: { (objects, key) -> Key in
			var keyʹ = key
			keyʹ.keyframe.objects = objects
			return keyʹ
		}, view: { (key) -> [Object.ID : Object] in
			return key.keyframe.objects
		})
	}
}


fileprivate extension Object {
	static func objectFromKeyframe(forID objID: Object.ID) -> Lens<Key, Object?> {
		return Key.keyframeObjects
			.composed(with: Dictionary<Object.ID, Object>.elementLens(for: objID))
	}
}

fileprivate extension Dictionary {
	static func elementLens(for key: Key) -> Lens<Dictionary, Value?> {
		return Lens<Dictionary, Value?>(set: { (value, dictionary) -> Dictionary in
			var dictionaryʹ = dictionary
			dictionaryʹ[key] = value
			return dictionaryʹ
		}, view: { (dictionary) -> Value? in
			return dictionary[key]
		})
	}
}
