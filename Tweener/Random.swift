import Foundation
import CoreGraphics

extension Float {
	static func random(within range: ClosedRange<Float>) -> Float {
		let normalizedRandom = Float(arc4random()) / Float(UInt32.max)
		return normalizedRandom * (range.upperBound - range.lowerBound) + range.lowerBound
	}
}

extension CGFloat {
	static func random(within range: ClosedRange<CGFloat>) -> CGFloat {
		let normalizedRandom = CGFloat(arc4random()) / CGFloat(UInt32.max)
		return normalizedRandom * (range.upperBound - range.lowerBound) + range.lowerBound
	}
}
