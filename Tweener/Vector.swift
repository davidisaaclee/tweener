import UIKit
import VectorSwift

extension Vector where Self.Iterator.Element: Ring {
	static var unit: Self {
		return self.init(collection: [Self.Iterator.Element.multiplicationIdentity,
		                              Self.Iterator.Element.multiplicationIdentity])
	}

	func dot<V: Vector>(_ otherVector: V) -> Self.Iterator.Element
		where V.Iterator.Element == Self.Iterator.Element, Self.Iterator.Element: Ring {
		return zip(self, otherVector)
			.map((*))
			.reduce(Self.Iterator.Element.additionIdentity, (+))
	}
}
