import UIKit

extension UIColor {
	static var appBlack: UIColor { return UIColor(red:0.01, green:0.09, blue:0.15, alpha:1.00) }
	static var appWhite: UIColor { return UIColor(red:0.99, green:1.00, blue:0.99, alpha:1.00) }
	static var appCyan: UIColor { return UIColor(red:0.24, green:0.77, blue:0.71, alpha:1.00) }
	static var appRed: UIColor { return UIColor(red:0.89, green:0.11, blue:0.23, alpha:1.00) }
	static var appLightRed: UIColor { return UIColor(red:0.96, green:0.67, blue:0.71, alpha:1.00) }
	static var appYellow: UIColor { return UIColor(red:0.99, green:0.62, blue:0.18, alpha:1.00) }
	static var appPurple: UIColor { return UIColor(hue:0.79, saturation:0.41, brightness:0.99, alpha:1.00) }
	static var appBlue: UIColor { return UIColor(hue:0.69, saturation:0.69, brightness:0.92, alpha:1.00) }
}
