import Foundation

protocol Codable {
	init?(coder: NSCoder)
	func encode(with coder: NSCoder)
}

extension NSCoder {
	func encode(_ codable: Codable, forKey key: String) {
		self.encode(NSKeyedArchiver.archivedData(withRootCodable: codable),
		            forKey: key)
	}

	func encode<T: Codable>(_ array: [T], forKey key: String) {
		self.encode(array.map { NSKeyedArchiver.archivedData(withRootCodable: $0) },
		            forKey: key)
	}

	func encode<T: Codable>(optional: T?, forKey key: String) {
		optional.tap { self.encode($0, forKey: key) }
	}

	func decode<T: Codable>(forKey key: String) -> T? {
		return self.decodeObject(forKey: key)
			.flatMap { $0 as? Data }
			.flatMap { NSKeyedUnarchiver.unarchiveCodable(with: $0) }
	}

	func decodeArray<T: Codable>(forKey key: String) -> [T]? {
		return self.decodeObject(forKey: key)
			.flatMap { $0 as? [Data] }
			.map {
				$0.flatMap { (data: Data) -> T? in 
					NSKeyedUnarchiver.unarchiveCodable(with: data)
				}
			}
	}

	func decodeOptional<T: Codable>(forKey key: String) -> T?? {
		guard self.containsValue(forKey: key) else {
			return .some(nil)
		}

		return self.decode(forKey: key)
	}
}

extension NSKeyedArchiver {
	static func archivedData(withRootCodable root: Codable) -> Data {
		let data = NSMutableData()
		let archiver = NSKeyedArchiver(forWritingWith: data)
		root.encode(with: archiver)
		archiver.finishEncoding()
		return data as Data
	}
}

extension NSKeyedUnarchiver {
	static func unarchiveCodable<T: Codable>(with data: Data) -> T? {
		let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
		let result = T.init(coder: unarchiver)
		unarchiver.finishDecoding()
		return result
	}
}
