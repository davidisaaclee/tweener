import Foundation

extension ComposerController: ProjectManagerDelegate {
	func projectManagerDidRequestNewProject(_ projectManager: ProjectManagerViewController) {
		ProjectDatabase.shared.createNewProject(name: "Project \(Date().description)")
			.onSuccess { _ in projectManager.reloadData() }
			.onFailure { print("Failed to create new project: \($0.localizedDescription)") }
	}

	func projectManager(_ projectManager: ProjectManagerViewController,
	                    didSelect project: Project,
	                    workspace: Workspace) {
		self.reset(with: workspace, as: project)
		self.navigationController
			.pushViewController(self.canvasEditor, animated: true)
	}

	func projectManager(_ projectManager: ProjectManagerViewController,
	                    didRequestDelete project: Project) {
		ProjectDatabase.shared.deleteWorkspace(for: project)
			.onFailure { print("Failed to delete project: \($0.localizedDescription)") }
			.onSuccess { _ in projectManager.reloadData() }
	}
}
