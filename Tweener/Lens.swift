import Foundation

struct Lens<Model, Property> {
	let set: (Property) -> (Model) -> Model
	let view: (Model) -> Property

	init(set: @escaping (Property) -> (Model) -> Model,
	     view: @escaping (Model) -> Property) {
		self.set = set
		self.view = view
	}

	init(set: @escaping (Property, Model) -> Model,
	     view: @escaping (Model) -> Property) {
		self.set = { property in { model in set(property, model) } }
		self.view = view
	}
}

extension Lens: LensType {}


protocol LensType {
	associatedtype PropertyType
	associatedtype ModelType

	var set: (PropertyType) -> (ModelType) -> ModelType { get }
	var view: (ModelType) -> PropertyType { get }
}

extension LensType {
	func over(_ transform: @escaping (PropertyType) -> PropertyType) -> (ModelType) -> ModelType {
		return { (model: ModelType) -> ModelType in
			self.set(transform(self.view(model)))(model)
		}
	}

	func composed<OtherLens: LensType>(with otherLens: OtherLens) -> Lens<ModelType, OtherLens.PropertyType> where Self.PropertyType == OtherLens.ModelType {
		return Lens<ModelType, OtherLens.PropertyType>(set: { (deepProperty, model) in
			return self.over(otherLens.set(deepProperty))(model)
		}, view: { (model) -> OtherLens.PropertyType in
			otherLens.view(self.view(model))
		})
	}

	/// Zips two lenses together into a tuple property type.
	/// When setting, `self` is applied first.
	func zip<OtherLens: LensType>(with otherLens: OtherLens) -> Lens<ModelType, (PropertyType, OtherLens.PropertyType)> where ModelType == OtherLens.ModelType {
		return Lens<ModelType, (PropertyType, OtherLens.PropertyType)>.init(set: { (properties, model) in
			let (myProperty, otherProperty) = properties
			return compose(self.set(myProperty), otherLens.set(otherProperty))(model)
		}, view: { (model) in
			return (self.view(model), otherLens.view(model))
		})
	}
}
