import Foundation

enum Interaction {
	typealias ID = UUID

	case scrub(ID, ScrubPointSet)
	case tap(id: ID, bounds: CirclePath)

	var id: ID {
		switch self {
		case let .scrub(id, _):
			return id

		case let .tap(id, _):
			return id
		}
	}
}

extension Interaction: Codable {
	init?(coder: NSCoder) {
		guard
			let associatedData = coder.decodeObject(forKey: "associatedData") as? Data,
			let typeTag = coder.decodeObject(forKey: "typeTag") as? String
			else {
				return nil
		}

		let subunarchiver = NSKeyedUnarchiver(forReadingWith: associatedData)

		guard let id: Interaction.ID = subunarchiver.decode(forKey: "id") else {
			return nil
		}

		switch typeTag {
		case "scrub":
			guard let scrubPointSet: ScrubPointSet = subunarchiver.decode(forKey: "scrubPointSet") else {
				return nil
			}
			self = .scrub(id, scrubPointSet)

		case "tap":
			guard let bounds: CirclePath = subunarchiver.decode(forKey: "bounds") else {
				return nil
			}
			self = .tap(id: id, bounds: bounds)

		default:
			return nil
		}

		subunarchiver.finishDecoding()
	}

	func encode(with coder: NSCoder) {
		var associatedData: Data {
			let data = NSMutableData()
			let subarchiver = NSKeyedArchiver(forWritingWith: data)

			switch self {
			case let .scrub(_, scrubPointSet):
				subarchiver.encode(scrubPointSet, forKey: "scrubPointSet")

			case let .tap(_, bounds):
				subarchiver.encode(bounds, forKey: "bounds")
			}

			subarchiver.encode(self.id, forKey: "id")

			subarchiver.finishEncoding()
			return data as Data
		}

		var typeTag: String {
			switch self {
			case .scrub:
				return "scrub"
				
			case .tap:
				return "tap"
			}
		}

		coder.encode(typeTag, forKey: "typeTag")
		coder.encode(associatedData, forKey: "associatedData")
	}
}
