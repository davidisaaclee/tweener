import Foundation

struct Key {
	typealias ID = UUID

	init(id: ID = UUID(), keyframe: Canvas) {
		self.id = id
		self.keyframe = keyframe
	}

	let id: ID
	var keyframe: Canvas
}

extension Key: Codable {
	init?(coder: NSCoder) {
		guard
			let id: UUID = coder.decode(forKey: "id"),
			let keyframe: Canvas = coder.decode(forKey: "keyframe")
			else {
				return nil
			}

		self.init(id: id, keyframe: keyframe)
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.id, forKey: "id")
		coder.encode(self.keyframe, forKey: "keyframe")
	}
}
