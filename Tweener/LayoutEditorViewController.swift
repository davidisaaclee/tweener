import UIKit
import VectorSwift
import AudioToolbox
import AVFoundation

protocol LayoutEditorDelegate: class {
	func layoutEditorDidTapPaletteButton(_ layoutEditor: LayoutEditorViewController)

	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didBeginTouchingObjectWithID objectID: UUID)
	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didDragObjectWithID objectID: UUID,
	                  dragAmount: CGPoint)
	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didEndTouchingObjectWithID objectID: UUID)

	func layoutEditorDidBeginRecording(_ layoutEditor: LayoutEditorViewController)
	func layoutEditorDidEndRecording(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidRequestSlowerPlayback(_ layoutEditor: LayoutEditorViewController)
	func layoutEditorDidRequestFasterPlayback(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidRequestGoToKeyframe(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidToggleClick(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidRequestEnterScrubMode(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidRequestResumePlayback(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidRequestGoBack(_ layoutEditor: LayoutEditorViewController)

	func layoutEditorDidRequestUndo(_ layoutEditor: LayoutEditorViewController)

	func layoutEditor(_ layoutEditor: LayoutEditorViewController,
	                  didRequestDeleteObjectWithID objectID: Object.ID)
}

class LayoutEditorViewController: UIViewController, EditorControls {

	enum ControlButton: Equatable {
		case record
		case play
		case openPalette
		case editInteractions
		case editKeyframe
		case hidden

		func render(on button: UIButton) {
			DispatchQueue.main.async {
				func setText(_ text: String) {
					UIView.performWithoutAnimation {
						button.setTitle(text, for: .normal)
						button.layoutIfNeeded()
					}
				}

				button.alpha = 1
				button.titleLabel?.font = .systemFont(ofSize: 26)

				switch self { 
				case .record:
					setText("🔴")

				case .play:
					setText("▶️")

				case .openPalette:
					setText("🎨")

				case .editInteractions:
					setText("🕹")

				case .editKeyframe:
					setText("◾️")

				case .hidden:
					button.alpha = 0
				}
			}
		}

		static func == (lhs: ControlButton, rhs: ControlButton) -> Bool {
			switch (lhs, rhs) {
			case (.record, .record):
				return true

			case (.play, .play):
				return true

			case (.openPalette, .openPalette):
				return true

			case (.editInteractions, .editInteractions):
				return true

			case (.editKeyframe, .editKeyframe):
				return true

			case (.hidden, .hidden):
				return true

			default:
				return false
			}
		}
	}

	weak var delegate: LayoutEditorDelegate?

	var convertPointToSceneFromView: ((CGPoint, UIView) -> CGPoint)?
	var convertPointFromSceneToView: ((CGPoint, UIView) -> CGPoint)?

	fileprivate var leftButtonModel: ControlButton = .hidden {
		didSet {
			guard oldValue != self.leftButtonModel else {
				return
			}

			self.leftButtonModel.render(on: self.leftButton)
		}
	}

	fileprivate var centerButtonModel: ControlButton = .hidden {
		didSet {
			guard oldValue != self.centerButtonModel else {
				return
			}

			self.centerButtonModel.render(on: self.centerButton)
		}
	}

	fileprivate var rightButtonModel: ControlButton = .hidden {
		didSet {
			guard oldValue != self.rightButtonModel else {
				return
			}

			self.rightButtonModel.render(on: self.rightButton)
		}
	}

	@IBOutlet weak var leftButton: UIButton! {
		didSet {
			self.leftButton.alpha = 0
		}
	}
	@IBOutlet weak var centerButton: UIButton! {
		didSet {
			self.centerButton.alpha = 0
		}
	}
	@IBOutlet weak var rightButton: UIButton! {
		didSet {
			self.rightButton.alpha = 0
		}
	}

	var clickSoundURL = URL(fileURLWithPath: Bundle.main.path(forResource: "click", ofType: "aif")!)
	var audioPlayer = AVAudioPlayer()

	@IBOutlet weak var paletteButton: UIButton!
	@IBOutlet weak var goBackButton: UIButton!
	@IBOutlet weak var slowerButton: UIButton!
	@IBOutlet weak var fasterButton: UIButton!
	@IBOutlet weak var deleteButton: UIButton!

	var isHoldingKnife: Bool = false

	// TODO: don't hold onto this
	fileprivate var workspace: Workspace?

	fileprivate var draggedObjectID: UUID?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.audioPlayer = try! AVAudioPlayer(contentsOf: self.clickSoundURL)
		self.audioPlayer.prepareToPlay()
	}

	/// Closes and prevents future events for active touches. New touches will fire events.
	func invalidateActiveTouches() {
		if let draggedObjectID = self.draggedObjectID {
			self.delegate?.layoutEditor(self,
			                            didEndTouchingObjectWithID: draggedObjectID)
		}

		self.touchState = nil
		self.draggedObjectID = nil
	}


	// MARK: View update

	func update(with workspace: Workspace) {
		self.workspace = workspace

		self.updateControlsVisibility(using: workspace)
	}

	private func updateControlsVisibility(using workspace: Workspace) {
		let layoutControls: Set<UIView> = [self.goBackButton,
		                                   self.deleteButton,
		                                   self.paletteButton]

		let playbackControls: Set<UIView> = [self.paletteButton,
		                                     self.slowerButton,
		                                     self.deleteButton,
		                                     self.fasterButton]

		let allControls: Set<UIView> =
			[layoutControls, playbackControls]
				.reduce([], { $0.union($1) })

		allControls.forEach { $0.isHidden = true }

		switch workspace.mode {
		case .layout:
			layoutControls.forEach { $0.isHidden = false }

		case .playback:
			playbackControls.forEach { $0.isHidden = false }

		case .editInteractions, .moveScrubPoint, .moveTapPoint, .overdub, .recording, .scrub, .tapped, .interact:
			break
		}

		switch workspace.mode {
		case .layout:
			self.leftButtonModel = .play
			self.centerButtonModel = .record
			self.rightButtonModel = .hidden

		case .recording:
			self.leftButtonModel = .hidden
			self.centerButtonModel = .record
			self.rightButtonModel = .hidden

		case .playback:
			self.leftButtonModel = .editKeyframe
			self.centerButtonModel = .record
			self.rightButtonModel = .editInteractions

		case .overdub:
			self.leftButtonModel = .hidden
			self.centerButtonModel = .record
			self.rightButtonModel = .hidden

		case .scrub, .tapped, .moveScrubPoint, .moveTapPoint, .editInteractions, .interact:
			break
		}
	}

	fileprivate func performDownAction(for button: ControlButton) {
		switch button {
		case .record:
			self.delegate?.layoutEditorDidBeginRecording(self)

		case .play:
			break

		case .openPalette:
			break

		case .editInteractions:
			break

		case .editKeyframe:
			break

		case .hidden:
			break
		}
	}

	fileprivate func performUpAction(for button: ControlButton) {
		switch button {
		case .record:
			self.delegate?.layoutEditorDidEndRecording(self)

		case .play:
			self.delegate?.layoutEditorDidRequestResumePlayback(self)

		case .openPalette:
			self.delegate?.layoutEditorDidTapPaletteButton(self)

		case .editInteractions:
			self.delegate?.layoutEditorDidRequestEnterScrubMode(self)

		case .editKeyframe:
			self.delegate?.layoutEditorDidRequestGoToKeyframe(self)

		case .hidden:
			break
		}
	}


	// MARK: UIResponder interaction

	private var touchState: (touch: UITouch, previousLocation: CGPoint)?

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard self.touchState == nil else {
			return
		}

		let touch = touches.first!
		let locationInView = touch.location(in: self.view)
		let locationInScene =
			self.convert(locationInView, toSceneFromView: self.view)
		self.touchState = (touch: touch, previousLocation: locationInScene)

		self.begin(touch, at: locationInScene)
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let touchState = self.touchState, touches.contains(touchState.touch) else {
			return
		}

		let locationInView = touchState.touch.location(in: self.view)
		let locationInScene =
			self.convert(locationInView, toSceneFromView: self.view)
		let locationDelta = locationInScene - touchState.previousLocation

		self.touchState = (touch: touchState.touch,
		                   previousLocation: locationInScene)

		self.move(touchState.touch, to: locationInScene)
		self.move(touchState.touch, by: locationDelta)
	}

	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesCancelled(touches, with: event)
		self.touchState = nil
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let touchState = self.touchState, touches.contains(touchState.touch) else {
			self.touchState = nil
			return
		}

		let locationInView = touchState.touch.location(in: self.view)
		let locationInScene =
			self.convert(locationInView, toSceneFromView: self.view)

		self.touchState = nil

		self.end(touchState.touch, at: locationInScene)
	}


	// MARK: Interaction

	func begin(_ touch: UITouch, at position: CGPoint) {
		guard let objects = self.workspace?.canvasBuffer.objects else {
			return
		}

		let touchedObjects = objects.values.filter { object in
			object.boundingBox.insetBy(dx: -20, dy: -20).contains(position)
		}

		guard let touchedObject = touchedObjects.first else {
			self.draggedObjectID = nil
			return
		}

		guard !self.isHoldingKnife else {
			self.delegate?.layoutEditor(self,
			                            didRequestDeleteObjectWithID: touchedObject.id)
			return
		}

		self.draggedObjectID = touchedObject.id
		self.delegate?.layoutEditor(self,
		                            didBeginTouchingObjectWithID: touchedObject.id)
	}

	func move(_ touch: UITouch, to position: CGPoint) {}

	func move(_ touch: UITouch, by positionDelta: CGPoint) {
		guard let draggedObjectID = self.draggedObjectID else {
			return
		}

		self.delegate?.layoutEditor(self,
		                            didDragObjectWithID: draggedObjectID,
		                            dragAmount: positionDelta)
	}

	func end(_ touch: UITouch, at position: CGPoint) {
		guard let draggedObjectID = self.draggedObjectID else {
			return
		}

		self.delegate?.layoutEditor(self,
		                            didEndTouchingObjectWithID: draggedObjectID)
		self.draggedObjectID = nil

	}


	// MARK: IBActions

	@IBAction func openPalette() {
		self.delegate?.layoutEditorDidTapPaletteButton(self)
	}

	@IBAction func playbackSlower() {
		self.delegate?.layoutEditorDidRequestSlowerPlayback(self)
	}

	@IBAction func playbackFaster() {
		self.delegate?.layoutEditorDidRequestFasterPlayback(self)
	}

	@IBAction func goBack() {
		self.delegate?.layoutEditorDidRequestGoBack(self)
	}

	@IBAction func handleDeleteButtonDown() {
		self.isHoldingKnife = true
	}

	@IBAction func handleDeleteButtonUp() {
		self.isHoldingKnife = false
	}

	@IBAction func handleLeftButtonDown() {
		self.performDownAction(for: self.leftButtonModel)
	}

	@IBAction func handleLeftButtonUp() {
		self.performUpAction(for: self.leftButtonModel)
	}

	@IBAction func handleCenterButtonDown() {
		self.performDownAction(for: self.centerButtonModel)
	}

	@IBAction func handleCenterButtonUp() {
		self.performUpAction(for: self.centerButtonModel)
	}

	@IBAction func handleRightButtonDown() {
		self.performDownAction(for: self.rightButtonModel)
	}

	@IBAction func handleRightButtonUp() {
		self.performUpAction(for: self.rightButtonModel)
	}


}
