import Foundation
import UIKit
import VectorSwift

class ComposerController {
	let navigationController: UINavigationController
	let canvasEditor: CanvasEditorViewController

	var workspace = Workspace(canvas: Canvas(objects: [:])) {
		didSet {
			self.swapControlsIfNeeded(from: oldValue.mode, to: self.workspace.mode)
		}
	}

	var project: Project!

	fileprivate var canvasUnsubscribeToken: UUID!

	fileprivate var displayLink: CADisplayLink!

	fileprivate var autosaveTimer: Timer!

	init(navigationController: UINavigationController,
	     canvasEditor: CanvasEditorViewController) {
		self.navigationController = navigationController
		self.canvasEditor = canvasEditor

		self.canvasUnsubscribeToken =
			self.workspace.addObserver(self.canvasEditor.update(with:))

		self.displayLink = CADisplayLink(target: self,
		                                 selector: #selector(ComposerController.tick))
		self.displayLink.add(to: RunLoop.main,
		                     forMode: .commonModes)

		let undoRecognizer =
			UITapGestureRecognizer(target: self,
			                       action: #selector(ComposerController.handleUndo(using:)))
		undoRecognizer.numberOfTapsRequired = 2
		undoRecognizer.numberOfTouchesRequired = 2
		self.navigationController.view.addGestureRecognizer(undoRecognizer)

		self.autosaveTimer =
			Timer.scheduledTimer(timeInterval: 60,
			                     target: self,
			                     selector: #selector(ComposerController.save),
			                     userInfo: nil,
			                     repeats: true)
	}

	func reset(with workspace: Workspace, as project: Project) {
		self.workspace = workspace
		self.project = project

		self.navigationController.popToRootViewController(animated: false)
		self.workspace.removeObserver(for: self.canvasUnsubscribeToken)
		self.canvasUnsubscribeToken =
			self.workspace.addObserver(self.canvasEditor.update(with:))

		self.workspace.mode = .layout
	}

	@objc private func handleUndo(using recognizer: UITapGestureRecognizer) {
		guard case .ended = recognizer.state else {
			return
		}

		self.workspace.undo()
	}

	@objc private func save() {
		guard self.navigationController.viewControllers.count > 1 else {
			// We don't care to autosave when in project manager.
			return
		}

		guard let project = self.project else {
			return
		}

		DispatchQueue.global(qos: .background).async {
			print("Autosaving...")
			ProjectDatabase.shared.save(self.workspace, as: project)
				.onFailure { print("Failed to autosave workspace: \($0.localizedDescription)") }
				.onSuccess { _ in print("Autosaved.") }
		}
	}


	func goToParent() {
		self.workspace.saveToHistory()
		self.workspace.allTransitions.values
			.filter { $0.toKeyID == self.workspace.currentKey.id }
			.first
			.map { $0.fromKeyID }
			.tap {
				self.workspace.currentKeyID = $0
				self.workspace.currentTransitionID =
					self.workspace.transitions(for: self.workspace.currentKey).first!.id
			}
			.otherwise {
				guard let project = self.project else {
					return
				}

				ProjectDatabase.shared.save(self.workspace, as: project)
					.onFailure { print("Failed to save workspace: \($0.localizedDescription)") }
					.onComplete { _ in self.navigationController.popViewController(animated: true) }
		}
	}

	fileprivate func swapControlsIfNeeded(from oldMode: Workspace.EditMode,
	                                      to newMode: Workspace.EditMode) {
		guard newMode != oldMode else {
			return
		}

		enum Controls {
			case layout
			case interactions
		}

		func controlType(for mode: Workspace.EditMode) -> Controls {
			switch mode {
			case .layout, .playback, .recording, .overdub:
				return .layout

			case .interact, .scrub, .tapped, .moveScrubPoint, .moveTapPoint, .editInteractions:
				return .interactions
			}
		}

		switch (controlType(for: oldMode), controlType(for: newMode)) {
		case (.interactions, .layout):
			self.canvasEditor.activate(self.makeLayoutEditor())

		case (.layout, .interactions):
			self.canvasEditor.activate(self.makeScrubPointControls())

		default:
			return
		}
	}

	// MARK: Making view controllers

	func makeProjectManager() -> ProjectManagerViewController {
		let manager = ProjectManagerViewController()
		manager.delegate = self
		return manager
	}

	func makeLayoutEditor() -> LayoutEditorViewController {
		let editor = LayoutEditorViewController()
		editor.delegate = self
		return editor
	}

	func makeScrubPointControls() -> ScrubPointsEditorViewController {
		let editor = ScrubPointsEditorViewController()
		editor.delegate = self
		return editor
	}

	func makeObjectPalette() -> UIViewController {
		let palette = ObjectPaletteViewController()
		palette.delegate = self
		return palette
	}


	// MARK: Update

	@objc private func tick() {
		let previousPlaybackOrNil = self.workspace.playback
		self.progressPlayback()
		let newPlaybackOrNil = self.workspace.playback

		self.render()

		switch self.workspace.mode {
		case .recording, .overdub:
			self.recordFrame(playback: newPlaybackOrNil,
			                 previousPlayback: previousPlaybackOrNil)

		default:
			break
		}
	}


	internal func progressPlayback() {
		guard let playback = self.workspace.playback else {
			return
		}

		guard let animation = workspace.currentAnimation else {
			return
		}

		var playbackʹ: Playback {
			switch self.workspace.mode {
			case .playback, .overdub:
				return playback.updated(at: Date(),
				                        animation: animation,
				                        isPlaying: true)

			case let .tapped(_, startDate):
				return playback.updated(at: Date(),
				                        startedAt: startDate,
				                        animation: animation)

			default:
				return playback.updated(at: Date(),
				                        animation: animation,
				                        isPlaying: false)
			}
		}

		self.workspace.playback = playbackʹ

		// If we hit the end of the transition...
		// TODO: Make better
		if playbackʹ.position > 0.9  {
			func progressToKey(withID toKeyID: Key.ID) {
				self.workspace.saveToHistory()

				guard let toKey = self.workspace.allKeys[toKeyID] else {
					return
				}

				let playback = Playback(fps: 60, position: 0)
				self.workspace.currentKeyID = toKeyID

				let nextTransitionID = self.workspace.transitions(for: toKey).first!.id
				self.workspace.currentTransitionID = nextTransitionID
				self.workspace.mode = .interact
				self.workspace.playback = playback
				self.workspace.canvasBuffer = self.render(playback)
			}

			switch self.workspace.mode {
			case .scrub:
				if let toKeyID = self.workspace.currentTransition?.toKeyID {
					progressToKey(withID: toKeyID)
				}

			case .tapped:
				if let toKeyID = self.workspace.currentTransition?.toKeyID {
					progressToKey(withID: toKeyID)
				} else {
					self.workspace.playback?.position = 0
				}
				self.workspace.mode = .interact

			default:
				break
			}
		}

		// If we looped...
		// TODO: Make better
		if playback.position > 0.9 && playbackʹ.position < 0.1 {
			switch self.workspace.mode {
			case .overdub:
				if !self.workspace.touchedObjectIDs.isEmpty {
					(self.canvasEditor.activeControls as? LayoutEditorViewController)?.invalidateActiveTouches()
					self.workspace.touchedObjectIDs.removeAll()
				}

			default:
				break
			}
		}
	}

	internal func recordFrame(at frameIndex: Int,
	                          lastWrittenFrameIndex: Int,
	                          previousAnimation: Animation) -> Animation {
		let touchedObjects: [UUID: Object] =
			self.workspace.touchedObjectIDs
				.flatMap { self.workspace.canvasBuffer.objects[$0] }
				.map { ($0.id, $0) }
				.reduce([:], { $0.appending($1) })

		let frame =
			Animation.Frame(diffs: touchedObjects.mapValues { (objID, obj) in
				Object.Diff(base: self.workspace.currentKey.keyframe.objects[objID]!,
				            target: obj)
			})

		var animation = previousAnimation
		animation.insert(frame, at: frameIndex)

		if lastWrittenFrameIndex != frameIndex {
			// We might need to fill in frames that were dropped.

			// We'll tween between this frame and the current frame.
			let lastWrittenFrame = animation.frames[lastWrittenFrameIndex]

			let numberOfFramesToFill =
				animation.numberOfLoopingFrames(from: lastWrittenFrameIndex,
				                                to: frameIndex)

			var fillInFrameIndex =
				animation.incrementLoopingFrameIndex(lastWrittenFrameIndex)

			var frameOffset: Float = 1

			var progress =
				frameOffset / Float(numberOfFramesToFill)

			while fillInFrameIndex != frameIndex {
				let interpolatedFrame =
					Animation.Frame.tween(between: lastWrittenFrame,
					                      and: frame,
					                      amount: progress)
				animation.insert(interpolatedFrame, at: fillInFrameIndex)

				fillInFrameIndex =
					animation.incrementLoopingFrameIndex(fillInFrameIndex)

				frameOffset += 1

				progress =
					frameOffset / Float(numberOfFramesToFill)
			}
		}
		
		return animation
	}


	internal func recordFrame(playback: Playback?,
	                             previousPlayback: Playback?) {
		switch self.workspace.mode {
		case let .recording(previousRecordingState):
			let newState = previousRecordingState.updated(with: Date())
			self.workspace.modifyCurrentAnimation(transform: { (animation) -> Animation in
				return recordFrame(at: newState.frameIndex,
				                   lastWrittenFrameIndex: previousRecordingState.frameIndex,
				                   previousAnimation: animation)
			})
			self.workspace.mode = .recording(state: newState)

		case .overdub:
			guard let playback = playback, let previousPlayback = previousPlayback else {
				return
			}

			self.workspace.modifyCurrentAnimation(transform: { (animation) -> Animation in
				return recordFrame(at: animation.frameIndex(for: playback),
				                   lastWrittenFrameIndex: animation.frameIndex(for: previousPlayback),
				                   previousAnimation: animation)
			})

			self.workspace.mode = .overdub

		case .layout, .playback, .editInteractions, .interact, .scrub, .tapped, .moveScrubPoint, .moveTapPoint:
			fatalError()
		}
	}


	// MARK: Rendering

	internal func render() {
		switch self.workspace.mode {
		case .layout:
			self.workspace.canvasBuffer = self.workspace.currentKey.keyframe

		case .overdub, .playback, .scrub, .tapped, .moveScrubPoint, .moveTapPoint, .editInteractions, .interact:
			guard let playback = self.workspace.playback else {
				return
			}

			self.workspace.canvasBuffer = self.render(playback)

		case .recording:
			// Just let the canvas buffer handle itself.
			break
		}
	}


	/// Returns a set of all objects in the workspace's canvas, affected by
	/// the specified frame.
	internal func renderObjects(using frame: Animation.Frame,
	                            from objectSet: [UUID: Object],
	                            excluding excludedObjectIDs: Set<UUID>) -> [UUID: Object] {
		let objectsToUpdate: [(key: Object.ID, value: Object)] = frame.diffs
			.filter { !excludedObjectIDs.contains($0.key) }
			.map { (id, objDiff) in
				(key: id, value: objDiff.apply(to: objectSet[id]!))
			}

		let shownObjectIDs =
			Set<Object.ID>(objectsToUpdate.map { $0.key } + excludedObjectIDs)

		var renderedObjects: [Object.ID: Object] = objectsToUpdate.reduce(objectSet) { (objects, kv) in
			var obj = kv.value
			obj.isHidden = false
			return objects.appending((kv.key, obj))
		}

		renderedObjects =
			Set(objectSet.keys).subtracting(shownObjectIDs)
				.reduce(renderedObjects) { acc, objID in
					return acc.withTransformedValue(at: objID) {
						var objCopy = $0
						objCopy.isHidden = true
						return objCopy
					}
		}

		return renderedObjects
	}

	// TODO: Provide animation as argument.
	internal func render(_ playback: Playback) -> Canvas {
		guard let animation = workspace.currentAnimation else {
			return workspace.canvasBuffer
		}

		return render(animation.interpolationInfo(for: playback))
	}

	internal func render(_ interpolation: Animation.Interpolation) -> Canvas {
		let excludedObjectIDs = self.workspace.touchedObjectIDs

		let startObjects =
			renderObjects(using: interpolation.startFrame,
			              from: self.workspace.currentKey.keyframe.objects,
			              excluding: excludedObjectIDs)
		let endObjects =
			renderObjects(using: interpolation.endFrame,
			              from: self.workspace.currentKey.keyframe.objects,
			              excluding: excludedObjectIDs)

		var objects: [Object.ID: Object] =
			startObjects.mapValues { (id, obj) in
				guard let endObject = endObjects[id] else {
					// Can probably just return `obj`, but curious when this happens...
					fatalError()
				}

				return Object.tween(between: obj,
				                    and: endObject,
				                    amount: interpolation.progress)
		}

		objects = excludedObjectIDs
			.reduce(objects) { (objects, touchedObjectID) in
				let kvPair = (touchedObjectID,
				              self.workspace.canvasBuffer.objects[touchedObjectID]!)
				return objects.appending(kvPair)
		}

		return Canvas(objects: objects)
	}

}

