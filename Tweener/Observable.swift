protocol Observable {
	associatedtype UnregisterToken
	associatedtype ObservedValue

	mutating func addObserver(_ callback: @escaping (ObservedValue) -> Void) -> UnregisterToken
	mutating func removeObserver(for token: UnregisterToken)
}
