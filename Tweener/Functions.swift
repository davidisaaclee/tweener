
/// Identity function.
func identity<A>(_ a: A) -> A { return a }

/// Compose functions mapping a type to the same type.
/// The left-most function is applied first.
func compose<A>(_ transforms: @escaping ((A) -> A)...) -> (A) -> A {
	return transforms.reduce({ $0 }) { acc, elm in
		return { input in elm(acc(input)) }
	}
}

precedencegroup ApplicationPrecedence {
	associativity: left
	higherThan: AssignmentPrecedence
}

infix operator <|: ApplicationPrecedence

func <| <A, B> (function: (A) -> B, value: A) -> B {
	return function(value)
}
