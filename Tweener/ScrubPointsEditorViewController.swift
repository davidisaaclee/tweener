import UIKit
import VectorSwift

protocol ScrubPointsEditorDelegate: class {
	func scrubPointsEditorDidTapDismiss(_ scrubPointsEditor: ScrubPointsEditorViewController)

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didBeginMovingStartScrubOfInteractionWithID interactionID: Interaction.ID)
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didBeginMovingEndScrubOfInteractionWithID interactionID: Interaction.ID)
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didFinishMovingScrubOfInteractionWithID interactionID: Interaction.ID)

	// Communicates scrub amount as an absolute value.
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didScrubInteractionWithID interactionID: Interaction.ID,
	                       to scrubDelta: Float)
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didFinishScrubbingInteractionWithID interactionID: Interaction.ID)
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didTapInteractionWithID interactionID: Interaction.ID)

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didMoveStartScrubPointOfInteractionWithID interactionID: Interaction.ID,
	                       by amount: CGSize)
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didMoveEndScrubPointOfInteractionWithID interactionID: Interaction.ID,
	                       by amount: CGSize)


	// Interaction management

	func scrubPointsEditorDidBeginEditingInteractions(_ scrubPointsEditor: ScrubPointsEditorViewController)
	func scrubPointsEditorDidFinishEditingInteractions(_ scrubPointsEditor: ScrubPointsEditorViewController)

	func scrubPointsEditorDidInsertInteraction(_ scrubPointsEditor: ScrubPointsEditorViewController)

	// TODO: Rename this
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didExtendInteractionWithID interactionID: Interaction.ID)
	func scrubPointsEditorRequestedGoToParent(_ scrubPointsEditor: ScrubPointsEditorViewController)

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didRequestDeleteInteractionWithID interactionID: Interaction.ID)
	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didRequestDeleteDestinationForInteractionWithID interactionID: Interaction.ID)

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didRequestChangeTypeOfInteractionWithID interactionID: Interaction.ID)
}

class ScrubPointsEditorViewController: UIViewController {
	weak var delegate: ScrubPointsEditorDelegate?

	var convertPointToSceneFromView: ((CGPoint, UIView) -> CGPoint)?
	var convertPointFromSceneToView: ((CGPoint, UIView) -> CGPoint)?

	@IBOutlet weak var scrubPointsView: ShapeSetView!
	@IBOutlet weak var dismissButton: UIButton!
	@IBOutlet weak var newInteractionButton: UIButton!

	fileprivate var workspace: Workspace?
	fileprivate var initialScrubLocation: CGPoint?
	fileprivate var isHoldingKnife: Bool = false
	fileprivate var isChangingInteractionType: Bool = false

	fileprivate enum TapState {
		case inactive
		case possible(Interaction.ID)
	}
	fileprivate var tapState: TapState = .inactive

	// MARK: IBActions

	@IBAction func handleTapDismiss() {
		self.delegate?.scrubPointsEditorDidTapDismiss(self)
	}

	@IBAction func enterEditInteractionsMode() {
		self.delegate?.scrubPointsEditorDidBeginEditingInteractions(self)
	}

	@IBAction func exitEditInteractionsMode() {
		self.delegate?.scrubPointsEditorDidFinishEditingInteractions(self)
	}

	@IBAction func insertInteraction() {
		self.delegate?.scrubPointsEditorDidInsertInteraction(self)
	}

	@IBAction func goToParent() {
		self.delegate?.scrubPointsEditorRequestedGoToParent(self)
	}

	@IBAction func enterChangeInteractionMode() {
		self.isChangingInteractionType = true
	}

	@IBAction func exitChangeInteractionMode() {
		self.isChangingInteractionType = false
	}

	@IBAction func enterDeleteMode() {
		self.isHoldingKnife = true
	}

	@IBAction func exitDeleteMode() {
		self.isHoldingKnife = false
	}

	// MARK: Interaction

	func begin(_ touch: UITouch, at position: CGPoint) {
		self.initialScrubLocation = position

		guard let workspace = self.workspace else {
			return
		}

		func handleScrubTouch(on scrubPointSet: ScrubPointSet, with id: Interaction.ID) {
			guard scrubPointSet.startPath.path.contains(position) else {
				return
			}

			if self.isHoldingKnife {
				self.delegate?.scrubPointsEditor(self,
				                                 didRequestDeleteInteractionWithID: id)
			} else if self.isChangingInteractionType {
				self.delegate?.scrubPointsEditor(self,
				                                 didRequestChangeTypeOfInteractionWithID: id)
			} else {
				self.delegate?.scrubPointsEditor(self,
				                                 didScrubInteractionWithID: id,
				                                 to: 0)
			}
		}

		func handleTapTouch(on tapBounds: CirclePath, with id: Interaction.ID) {
			guard tapBounds.path.contains(position) else {
				return
			}

			if self.isHoldingKnife {
				self.delegate?.scrubPointsEditor(self,
				                                 didRequestDeleteInteractionWithID: id)
			} else if self.isChangingInteractionType {
				self.delegate?.scrubPointsEditor(self,
				                                 didRequestChangeTypeOfInteractionWithID: id)
			} else {
				self.tapState = .possible(id)
			}
		}

		workspace.transitions(for: workspace.currentKey)
			.forEach { (transition) in
				switch transition.interaction {
				case let .scrub(id, scrubPointSet):
					handleScrubTouch(on: scrubPointSet, with: id)

				case let .tap(id, bounds):
					handleTapTouch(on: bounds, with: id)
				}
			}
	}

	func move(_ touch: UITouch, to position: CGPoint) {
		guard !self.isHoldingKnife else {
			return
		}

		guard let workspace = self.workspace else {
			return
		}

		switch workspace.mode {
		case let .scrub(transitionID):
			guard let initialScrubLocation = self.initialScrubLocation else {
				return
			}

			guard case let .some(.scrub(id, scrubPoints)) = self.workspace?.allTransitions[transitionID]?.interaction else {
				return
			}

			let displacement = position - initialScrubLocation

			let projectionMagnitude =
				displacement.dot(scrubPoints.displacement * (1.0 / scrubPoints.displacement.magnitude))

			let scrubAmount = scrubPoints.displacement.magnitude == 0
				? 0
				: Float(projectionMagnitude / scrubPoints.displacement.magnitude)

			guard (0.0 ... 1.0).contains(scrubAmount) else {
				return
			}
			self.delegate?.scrubPointsEditor(self,
			                                 didScrubInteractionWithID: id,
			                                 to: scrubAmount)

			if touch.force > 6.5 {
				if scrubPoints.startPath.path.contains(position) {
					self.delegate?.scrubPointsEditor(self,
					                                 didBeginMovingStartScrubOfInteractionWithID: id)
				} else if scrubPoints.endPath.contains(position) {
					self.delegate?.scrubPointsEditor(self,
					                                 didBeginMovingEndScrubOfInteractionWithID: id)
				}
			}

		case .moveScrubPoint:
			break

		case .interact:
			func handleTapMove(id: Interaction.ID, tapBounds: CirclePath) {
				if touch.force > 6.5 {
					if tapBounds.path.contains(position) {
						self.delegate?.scrubPointsEditor(self,
						                                 didBeginMovingStartScrubOfInteractionWithID: id)
					}
				}
			}

			workspace.transitions(for: workspace.currentKey)
				.forEach { (transition) in
					switch transition.interaction {
					case let .tap(id, bounds):
						handleTapMove(id: id, tapBounds: bounds)

					default:
						break
					}
				}

		default:
			break
		}
	}

	func move(_ touch: UITouch, by positionDelta: CGPoint) {
		guard !self.isHoldingKnife else {
			return
		}

		guard let workspace = self.workspace else {
			return
		}

		switch workspace.mode {
		case let .moveScrubPoint(interactionID, isMovingStartPoint) where isMovingStartPoint:
			self.delegate?.scrubPointsEditor(self,
			                                 didMoveStartScrubPointOfInteractionWithID: interactionID,
			                                 by: CGSize(collection: positionDelta))

		case let .moveScrubPoint(interactionID, isMovingStartPoint) where !isMovingStartPoint:
			self.delegate?.scrubPointsEditor(self,
			                                 didMoveEndScrubPointOfInteractionWithID: interactionID,
			                                 by: CGSize(collection: positionDelta))

		case let .moveTapPoint(interactionID):
			self.delegate?.scrubPointsEditor(self,
			                                 didMoveStartScrubPointOfInteractionWithID: interactionID,
			                                 by: CGSize(collection: positionDelta))

		default:
			break
		}
	}

	func end(_ touch: UITouch, at position: CGPoint) {
		guard let workspace = self.workspace else {
			return
		}

		guard let currentInteraction = workspace.currentInteraction else {
			return
		}

		switch workspace.mode {
		case .interact:
			if case let .possible(possibleInteractionID) = self.tapState {
				_ = workspace.transitions(for: workspace.currentKey)
					.flatMap { (transition) -> (id: Interaction.ID, isHit: Bool)? in
						switch transition.interaction {
						case let .tap(id, bounds):
							return (id: id, isHit: bounds.path.contains(position))

						default:
							return nil
						}
					}
					.filter { $0.isHit }
					.first
					.tap {
						self.delegate?.scrubPointsEditor(self,
						                                 didTapInteractionWithID: $0.id)
					}
			}
			self.tapState = .inactive

		case .scrub:
			self.delegate?.scrubPointsEditor(self,
			                                 didFinishScrubbingInteractionWithID: currentInteraction.id)

		case .moveScrubPoint:
			self.delegate?.scrubPointsEditor(self,
			                                 didFinishMovingScrubOfInteractionWithID: currentInteraction.id)

		case .moveTapPoint:
			self.delegate?.scrubPointsEditor(self,
			                                 didFinishMovingScrubOfInteractionWithID: currentInteraction.id)

		case .editInteractions:
			func handleScrubEdit(on scrubPointSet: ScrubPointSet, with id: Interaction.ID) {
				guard scrubPointSet.endPath.contains(position) else {
					return
				}

				if self.isHoldingKnife {
					delegate?.scrubPointsEditor(self,
					                            didRequestDeleteDestinationForInteractionWithID: id)
				} else {
					delegate?.scrubPointsEditor(self,
					                            didExtendInteractionWithID: id)
				}
			}

			func handleTapEdit(on tapBounds: CirclePath, with id: Interaction.ID) {
				guard tapBounds.path.contains(position) else {
					return
				}

				if self.isHoldingKnife {
					delegate?.scrubPointsEditor(self,
					                            didRequestDeleteDestinationForInteractionWithID: id)
				} else {
					delegate?.scrubPointsEditor(self,
					                            didExtendInteractionWithID: id)
				}
			}

			workspace.transitions(for: workspace.currentKey)
				.forEach { (transition) in
					switch transition.interaction {
					case let .scrub(id, scrubPointSet):
						handleScrubEdit(on: scrubPointSet, with: id)

					case let .tap(id, bounds):
						handleTapEdit(on: bounds, with: id)
					}
				}

		default:
			break
		}
	}


	// MARK: UIResponder interaction

	fileprivate var touchState: (touch: UITouch, previousLocation: CGPoint)?

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard self.touchState == nil else {
			return
		}

		let touch = touches.first!
		let locationInView = touch.location(in: self.view)
		let locationInScene =
			self.convert(locationInView, toSceneFromView: self.view)
		self.touchState = (touch: touch, previousLocation: locationInScene)

		self.begin(touch, at: locationInScene)
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let touchState = self.touchState, touches.contains(touchState.touch) else {
			return
		}

		let locationInView = touchState.touch.location(in: self.view)
		let locationInScene =
			self.convert(locationInView, toSceneFromView: self.view)
		let locationDelta = locationInScene - touchState.previousLocation

		self.touchState = (touch: touchState.touch,
		                   previousLocation: locationInScene)

		self.move(touchState.touch, to: locationInScene)
		self.move(touchState.touch, by: locationDelta)
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let touchState = self.touchState, touches.contains(touchState.touch) else {
			return
		}

		let locationInView = touchState.touch.location(in: self.view)
		let locationInScene =
			self.convert(locationInView, toSceneFromView: self.view)

		self.touchState = nil

		self.end(touchState.touch, at: locationInScene)
	}
}

extension ScrubPointsEditorViewController: EditorControls {
	func update(with workspace: Workspace) {
		self.workspace = workspace

		switch workspace.mode {
		case .scrub, .interact:
			self.dismissButton.isHidden = false
			self.newInteractionButton.isHidden = true

		case .moveScrubPoint:
			self.dismissButton.isHidden = false
			self.newInteractionButton.isHidden = true

		case .editInteractions:
			self.dismissButton.isHidden = true
			self.newInteractionButton.isHidden = false

		default:
			break
		}

		updateScrubPoints(using: workspace)
	}

	private func updateScrubPoints(using workspace: Workspace) {
		defer { self.scrubPointsView.setNeedsDisplay() }

		self.scrubPointsView.shapes = [:]

		workspace
			.transitions(for: workspace.currentKey)
			.forEach { transition in
				switch transition.interaction {
				case let .scrub(id, scrubPointSet):
					draw(scrubPoints: scrubPointSet,
					     for: id,
					     linkingTo: transition.toKeyID,
					     from: workspace)

				case let .tap(id, bounds):
					draw(tapBounds: bounds,
					     for: id,
					     linkingTo: transition.toKeyID,
					     from: workspace)
				}
			}
	}

	private func draw(scrubPoints: ScrubPointSet,
	                  for id: Interaction.ID,
	                  linkingTo toKeyID: Key.ID?,
	                  from workspace: Workspace) {
		let startCenter =
			self.convert(scrubPoints.startPath.center,
			             fromSceneToView: self.view)
		let endCenter: CGPoint =
			self.convert(scrubPoints.startPath.center + scrubPoints.displacement,
			             fromSceneToView: self.view)

		let startScrubPointPath =
			UIBezierPath(circleWithRadius: scrubPoints.startPath.radius,
			             center: startCenter)
		let endScrubPointPath =
			UIBezierPath(circleWithRadius: scrubPoints.startPath.radius,
			             center: endCenter)

		startScrubPointPath.lineWidth = 6
		endScrubPointPath.lineWidth = 2

		let lineDash: [CGFloat] = [3, 5]

		let phase =
			(1 - Date().timeIntervalSinceReferenceDate.truncatingRemainder(dividingBy: 1)) * Double(lineDash.reduce(0, (+)))

		endScrubPointPath.setLineDash(lineDash, count: 2, phase: CGFloat(phase))
		startScrubPointPath.lineCapStyle = .round
		endScrubPointPath.lineCapStyle = .round

		let connectorPath =
			UIBezierPath()
		connectorPath.move(to: startCenter)
		connectorPath.addLine(to: endCenter)
		connectorPath.lineWidth = 2
		connectorPath.setLineDash(lineDash, count: 2, phase: CGFloat(phase))

		let isActiveInteraction =
			workspace.currentInteraction?.id == id

		let inactiveColor =
			UIColor(hue: 0.98,
			        saturation: 0.63,
			        brightness: 0.92,
			        alpha:1.00)

		var shapes =
			["startZone-\(id)": PathShape(path: startScrubPointPath,
			                              fillColor: nil,
			                              strokeColor: isActiveInteraction ? .white : inactiveColor,
			                              alpha: 0.5),
			 "endZone-\(id)": PathShape(path: endScrubPointPath,
			                            fillColor: nil,
			                            strokeColor: isActiveInteraction ? .white : inactiveColor,
			                            alpha: 0.5),
			 "connector-\(id)": PathShape(path: connectorPath,
			                              fillColor: nil,
			                              strokeColor: isActiveInteraction ? .white : inactiveColor,
			                              alpha: 0.5)]

		if case .editInteractions = workspace.mode {
			if toKeyID == nil {
				shapes["extendButton-\(id)"] =
					PathShape(path: endScrubPointPath,
					          fillColor: .appCyan,
					          strokeColor: nil)
			} else {
				shapes["extendButton-\(id)"] =
					PathShape(path: endScrubPointPath,
					          fillColor: .appRed,
					          strokeColor: nil)
			}
		}

		if case let .moveScrubPoint(interactionID, isMovingStartPoint) = workspace.mode {
			if interactionID == id {
				let selectionPath: CirclePath
				if isMovingStartPoint {
					selectionPath =
						CirclePath(center: startCenter,
						           radius: scrubPoints.startPath.radius + 8)
				} else {
					selectionPath =
						CirclePath(center: endCenter,
						           radius: scrubPoints.startPath.radius + 8)
				}

				let selectionBezierPath = selectionPath.path
				selectionBezierPath.lineWidth = 2

				shapes["selectionOutline-\(id)"] =
					PathShape(path: selectionPath.path,
					          fillColor: nil,
					          strokeColor: .white,
					          alpha: 0.5)
			}
		}

		if isActiveInteraction, let playback = workspace.playback {
			let progressPosition =
				startCenter + (endCenter - startCenter) * CGFloat(playback.position)
			let progressIndicatorPath =
				UIBezierPath(circleWithRadius: 5, center: progressPosition)

			shapes["progressIndicator-\(id)"] =
				PathShape(path: progressIndicatorPath,
				          fillColor: .white,
				          strokeColor: nil,
				          alpha: 0.5)
		}

		self.scrubPointsView.shapes =
			(self.scrubPointsView.shapes ?? [:])
				.merged(with: shapes.mapValues { _, shape in ShapeSetView.Shape(baseDrawable: shape, baseBounded: shape) })
	}

	private func draw(tapBounds: CirclePath,
	                  for id: Interaction.ID,
	                  linkingTo toKeyID: Key.ID?,
	                  from workspace: Workspace) {
		let tapBoundsCenter =
			self.convert(tapBounds.center,
			             fromSceneToView: self.view)
		let tapBoundsPath =
			UIBezierPath(circleWithRadius: tapBounds.radius,
			             center: tapBoundsCenter)

		tapBoundsPath.lineWidth = 6
		tapBoundsPath.lineCapStyle = .round

		let isActiveInteraction =
			workspace.currentInteraction?.id == id

		let inactiveColor =
			UIColor(hue: 0.98,
			        saturation: 0.63,
			        brightness: 0.92,
			        alpha:1.00)

		var shapes =
			["tapZone-\(id)": PathShape(path: tapBoundsPath,
			                              fillColor: nil,
			                              strokeColor: isActiveInteraction ? .white : inactiveColor,
			                              alpha: 0.5)]

		if case .editInteractions = workspace.mode {
			if toKeyID == nil {
				shapes["extendButton-\(id)"] =
					PathShape(path: tapBoundsPath,
					          fillColor: .appCyan,
					          strokeColor: nil)
			} else {
				shapes["extendButton-\(id)"] =
					PathShape(path: tapBoundsPath,
					          fillColor: .appRed,
					          strokeColor: nil)
			}
		}

		self.scrubPointsView.shapes =
			(self.scrubPointsView.shapes ?? [:])
				.merged(with: shapes.mapValues { _, shape in ShapeSetView.Shape(baseDrawable: shape, baseBounded: shape) })
	}
}
