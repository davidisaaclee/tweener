import UIKit

protocol ObjectPaletteDelegate: class {
	func objectPaletteDidTapHomeButton(_ objectPalette: ObjectPaletteViewController)
	func objectPalette(_ objectPalette: ObjectPaletteViewController,
	                   didChooseObject object: Object)
}

private let reuseIdentifier = "EmojiCell"

class ObjectPaletteViewController: UIViewController {
	weak var delegate: ObjectPaletteDelegate?

	@IBOutlet weak var emojiCollectionView: UICollectionView! {
		didSet {
			self.emojiCollectionView.register(UINib(nibName: "EmojiCollectionViewCell",
			                                        bundle: nil),
			                                  forCellWithReuseIdentifier: reuseIdentifier)
			self.emojiCollectionView.dataSource = self
			self.emojiCollectionView.delegate = self
		}
	}

	@IBOutlet weak var canvas: UIView!

	fileprivate let emoji = [
		"😎", "🍎", "🍋",
		"🍊", "🔑", "💯",
		"👻", "👁", "👉",
		"👾", "🚀", "❤️"
	]
	
	@IBAction func goHome() {
		self.delegate?.objectPaletteDidTapHomeButton(self)
	}
}

extension ObjectPaletteViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.emoji.count
	}

	func collectionView(_ collectionView: UICollectionView,
	                    cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let anyCell =
			collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
			                                   for: indexPath)

		guard let cell = anyCell as? EmojiCollectionViewCell else {
			return anyCell
		}

		cell.emoji = self.emoji[indexPath.item]
		return cell
	}
}

extension ObjectPaletteViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView,
	                    didSelectItemAt indexPath: IndexPath) {
		let obj = Object(content: .text(self.emoji[indexPath.item], k.defaultFont),
		                 position: .zero)
		self.delegate?.objectPalette(self, didChooseObject: obj)
	}
}
