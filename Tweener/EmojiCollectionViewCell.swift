import UIKit

class EmojiCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var label: UILabel!

	var emoji: String? {
		didSet {
			self.label?.text = self.emoji
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		self.label.text = self.emoji
	}
}
