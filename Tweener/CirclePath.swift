import UIKit

struct CirclePath {
	var center: CGPoint
	var radius: CGFloat

	var path: UIBezierPath {
		return UIBezierPath(ovalIn: CGRect(x: self.center.x - self.radius,
		                                   y: self.center.y - self.radius,
		                                   width: self.radius * 2,
		                                   height: self.radius * 2))
	}

	init(center: CGPoint, radius: CGFloat) {
		self.center = center
		self.radius = radius
	}
}


extension CirclePath: Codable {
	init?(coder: NSCoder) {
		self.init(center: coder.decodeCGPoint(forKey: "center"),
		          radius: CGFloat(coder.decodeFloat(forKey: "radius")))
	}

	func encode(with coder: NSCoder) {
		coder.encode(self.center, forKey: "center")
		coder.encode(Float(self.radius), forKey: "radius")
	}
}
