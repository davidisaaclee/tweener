import Foundation
import VectorSwift

extension ComposerController: ScrubPointsEditorDelegate {
	func scrubPointsEditorDidTapDismiss(_ scrubPointsEditor: ScrubPointsEditorViewController) {
		self.workspace.saveToHistory()

		if self.workspace.currentTransition == nil {
			if let nextTransition = self.workspace.transitions(for: self.workspace.currentKey).first {
				self.workspace.currentTransitionID = nextTransition.id
			} else {
				let newTransition = self.workspace.addTransition(from: workspace.currentKeyID)
				self.workspace.currentTransitionID = newTransition.id
			}
		}

		guard let currentAnimation = workspace.currentTransition?.animation else {
			print("Could not add or get current transition's animation.")
			return
		}

		if currentAnimation.frames.isEmpty {
			self.workspace.mode = .layout
		} else {
			self.workspace.mode = .playback
		}
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didScrubInteractionWithID interactionID: Interaction.ID,
	                       to position: Float) {
		let transitionOrNil = self.workspace.allTransitions.values
			.filter { $0.interaction.id == interactionID }
			.first

		guard let transition = transitionOrNil else {
			return
		}

		self.workspace.currentTransitionID = transition.id
		self.workspace.mode = .scrub(activeTransitionID: transition.id)
		let fps = self.workspace.playback?.fps ?? 60

		let playback = Playback(fps: fps, position: position)
		self.workspace.playback = playback
		self.workspace.canvasBuffer = self.render(playback)
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didFinishScrubbingInteractionWithID interactionID: Interaction.ID) {
		workspace.playback?.position = 0
		workspace.mode = .interact
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didTapInteractionWithID interactionID: Interaction.ID) {
		guard case .interact = self.workspace.mode else {
			return
		}

		let transitionOrNil = self.workspace.allTransitions.values
			.filter { $0.interaction.id == interactionID }
			.first

		guard let transition = transitionOrNil else {
			return
		}

		self.workspace.currentTransitionID = transition.id

		guard workspace.currentAnimation.map({ !$0.frames.isEmpty }) ?? false else {
			return
		}

		workspace.playback?.position = 0
		self.workspace.mode =
			.tapped(activeTransitionID: transition.id, startDate: Date())
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didMoveStartScrubPointOfInteractionWithID interactionID: Interaction.ID,
	                       by amount: CGSize) {
		self.workspace.modifyInteraction(withID: interactionID) { (interaction) in
			switch interaction {
			case let .scrub(id, scrubPointSet):
				var scrubPointSetʹ = scrubPointSet
				scrubPointSetʹ.startPath.center =
					scrubPointSet.startPath.center + amount

				scrubPointSetʹ.displacement =
					scrubPointSet.displacement - amount

				return .scrub(id, scrubPointSetʹ)

			case let .tap(id, bounds):
				var boundsʹ = bounds
				boundsʹ.center =
					bounds.center + amount
				return .tap(id: id, bounds: boundsʹ)
			}
		}
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didMoveEndScrubPointOfInteractionWithID interactionID: Interaction.ID,
	                       by amount: CGSize) {
		self.workspace.modifyInteraction(withID: interactionID) { (interaction) in
			switch interaction {
			case let .scrub(id, scrubPointSet):
				var scrubPointSetʹ = scrubPointSet
				scrubPointSetʹ.displacement =
					scrubPointSet.displacement + amount
				return .scrub(id, scrubPointSetʹ)

			case .tap:
				fatalError()
			}
		}
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didBeginMovingStartScrubOfInteractionWithID interactionID: Interaction.ID) {
		self.workspace.allTransitions
			.values
			.map { $0.interaction }
			.filter { $0.id == interactionID }
			.first
			.tap { (interaction) in
				switch interaction {
				case .scrub:
					switch self.workspace.mode {
					case .scrub, .interact:
						self.workspace.saveToHistory()
						self.workspace.mode =
							.moveScrubPoint(interactionID: interactionID, isMovingStartPoint: true)

					default:
						break
					}

				case .tap:
					guard case .interact = self.workspace.mode else {
						return
					}

					self.workspace.saveToHistory()
					self.workspace.mode =
						.moveTapPoint(interactionID: interactionID)
				}
			}
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didBeginMovingEndScrubOfInteractionWithID interactionID: Interaction.ID) {
		guard case .scrub = self.workspace.mode else {
			return
		}
		self.workspace.saveToHistory()

		self.workspace.allTransitions
			.values
			.map { $0.interaction }
			.filter { $0.id == interactionID }
			.first
			.tap { interaction in
				switch interaction {
				case let .scrub(id, _):
					self.workspace.mode = .moveScrubPoint(interactionID: id,
					                                      isMovingStartPoint: false)

				default:
					break
				}
			}
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didFinishMovingScrubOfInteractionWithID interactionID: Interaction.ID) {
		self.workspace.mode = .interact
	}


	// Interaction management

	func scrubPointsEditorDidBeginEditingInteractions(_ scrubPointsEditor: ScrubPointsEditorViewController) {
		self.workspace.mode = .editInteractions
	}

	func scrubPointsEditorDidFinishEditingInteractions(_ scrubPointsEditor: ScrubPointsEditorViewController) {
		self.workspace.mode = .interact
	}

	func scrubPointsEditorDidInsertInteraction(_ scrubPointsEditor: ScrubPointsEditorViewController) {
		self.workspace.saveToHistory()

		self.workspace.playback = nil

		workspace.canvasBuffer = workspace.currentKey.keyframe

		let newTransition =
			self.workspace.addTransition(from: self.workspace.currentKey.id)
		self.workspace.currentTransitionID = newTransition.id
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didExtendInteractionWithID interactionID: Interaction.ID) {
		self.workspace.saveToHistory()

		// TODO: this is a bad implementation, make it better

		let previousTransitionOrNil = self.workspace.allTransitions.values
			.filter { $0.interaction.id == interactionID }
			.first

		guard var previousTransition = previousTransitionOrNil else {
			return
		}

		let newKey: Key
		if let destinationKeyID = previousTransition.toKeyID {
			newKey = self.workspace.allKeys[destinationKeyID]!
			self.workspace.currentTransitionID =
				self.workspace.transitions(for: newKey).first!.id
		} else {
			guard let frame = previousTransition.animation.frames.last else {
				fatalError("Branched an empty animation")
			}

			let newKeyframeObjects =
				self.renderObjects(using: frame,
				                   from: self.workspace.currentKey.keyframe.objects,
				                   excluding: [])

			newKey = Key(keyframe: Canvas(objects: newKeyframeObjects))
			self.workspace.insert(newKey)

			let firstTransition = self.workspace.addTransition(from: newKey.id)
			self.workspace.currentTransitionID = firstTransition.id

			// Update the "to" of the transition linking to the new key.
			previousTransition.toKeyID = newKey.id
			self.workspace.allTransitions[previousTransition.id] = previousTransition
		}

		self.workspace.currentKeyID = newKey.id

		if self.workspace.currentTransition!.animation.frames.isEmpty {
			self.workspace.mode = .layout
			self.workspace.playback = nil
		} else {
			self.workspace.mode = .playback
		}
	}

	func scrubPointsEditorRequestedGoToParent(_ scrubPointsEditor: ScrubPointsEditorViewController) {
		self.goToParent()
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didRequestDeleteInteractionWithID interactionID: Interaction.ID) {
		guard let transition = self.workspace.allTransitions.values.filter({ $0.interaction.id == interactionID }).first else {
			return
		}

		self.workspace.saveToHistory()
		self.workspace =
			self.workspace.deletingTransition(withID: transition.id)
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didRequestDeleteDestinationForInteractionWithID interactionID: Interaction.ID) {
		guard let transition = self.workspace.allTransitions.values.filter({ $0.interaction.id == interactionID }).first else {
			return
		}

		guard let keyID = transition.toKeyID else {
			return
		}

		self.workspace.saveToHistory()
		self.workspace =
			self.workspace.deletingKey(withID: keyID)
	}

	func scrubPointsEditor(_ scrubPointsEditor: ScrubPointsEditorViewController,
	                       didRequestChangeTypeOfInteractionWithID interactionID: Interaction.ID) {
		self.workspace.modifyInteraction(withID: interactionID) { (interaction) in
			switch interaction {
			case let .scrub(id, scrubPoints):
				return .tap(id: id, bounds: scrubPoints.startPath)

			case let .tap(id, tapBounds):
				let displacement: CGSize =
					CGSize(collection: tapBounds.center) * -2
				return .scrub(id,
				              ScrubPointSet(startPath: tapBounds,
				                            displacement: displacement))
			}
		}
	}
}
