import UIKit
import VectorSwift

struct PathShape {
	let path: UIBezierPath
	let fillColor: UIColor?
	let strokeColor: UIColor?

	let blendMode: CGBlendMode
	let alpha: CGFloat

	init(path: UIBezierPath,
	     fillColor: UIColor?,
	     strokeColor: UIColor?,
	     blendMode: CGBlendMode = .normal,
	     alpha: CGFloat = 1) {
		self.path = path
		self.fillColor = fillColor
		self.strokeColor = strokeColor
		self.blendMode = blendMode
		self.alpha = alpha
	}
}

extension PathShape: Drawable {
	func draw(_ rect: CGRect) {
		if let fillColor = self.fillColor {
			fillColor.setFill()
			self.path.fill(with: self.blendMode, alpha: self.alpha)
		}

		if let strokeColor = self.strokeColor {
			strokeColor.setStroke()
			self.path.stroke(with: self.blendMode, alpha: self.alpha)
		}
	}
}

extension PathShape: Bounded {
	var bounds: CGRect {
		 return self.path.bounds
	}
}

protocol Drawable {
	func draw(_ rect: CGRect)
}

protocol Bounded {
	var bounds: CGRect { get }
}


protocol ShapeSetViewDelegate: class {
	func shapeSetView(_ shapeSetView: ShapeSetView,
	                  didTouchDownOn shape: ShapeSetView.Shape,
	                  withID id: ShapeSetView.ShapeID)

	func shapeSetView(_ shapeSetView: ShapeSetView,
	                  didMoveTouchFrom shape: ShapeSetView.Shape,
	                  withID id: ShapeSetView.ShapeID,
	                  by moveAmount: CGSize)

	func shapeSetView(_ shapeSetView: ShapeSetView,
	                  didMoveTouchFrom shape: ShapeSetView.Shape,
	                  withID id: ShapeSetView.ShapeID,
	                  displacement: CGSize)

	func shapeSetView(_ shapeSetView: ShapeSetView,
	                  didTouchUpFrom shape: ShapeSetView.Shape,
	                  withID id: ShapeSetView.ShapeID)
}

class ShapeSetView: UIView {
	struct Shape: Drawable, Bounded {
		let baseDrawable: Drawable
		let baseBounded: Bounded

		var bounds: CGRect {
			return self.baseBounded.bounds
		}

		init(baseDrawable: Drawable, baseBounded: Bounded) {
			self.baseDrawable = baseDrawable
			self.baseBounded = baseBounded
		}

		func draw(_ rect: CGRect) {
			self.baseDrawable.draw(rect)
		}
	}
	typealias ShapeID = String

	weak var delegate: ShapeSetViewDelegate?

	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setup()
	}

	private func setup() {
		self.backgroundColor = .clear
	}

	var shapes: [ShapeID: ShapeSetView.Shape]?

	private var touchToShape: [UITouch: (id: ShapeID, initialTouchLocation: CGPoint, previousTouchLocation: CGPoint)] = [:]

	override func draw(_ rect: CGRect) {
		super.draw(rect)

		guard let shapes = self.shapes else {
			return
		}

		shapes.values.forEach { (shape: ShapeSetView.Shape) in shape.draw(rect) }
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesBegan(touches, with: event)

		guard let shapes = self.shapes else {
			return
		}

		func hitTest(touch: UITouch) -> ShapeID? {
			for (id, shape) in shapes {
				if shape.bounds.contains(touch.location(in: self)) {
					self.delegate?.shapeSetView(self, didTouchDownOn: shape, withID: id)
					return id
				}
			}

			return nil
		}

		for touch in touches {
			if let hitShapeID = hitTest(touch: touch) {
				let touchLocation = touch.location(in: self)
				self.touchToShape[touch] = (id: hitShapeID,
				                            initialTouchLocation: touchLocation,
				                            previousTouchLocation: touchLocation)
			}
		}
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesMoved(touches, with: event)

		guard let shapes = self.shapes else {
			return
		}

		self.touchToShape = self.touchToShape.mapValues { (touch, info) in
			let (shapeID, initialTouchLocation, previousTouchLocation) = info
			let touchLocation = touch.location(in: self)
			let displacementDelta = touchLocation - previousTouchLocation

			self.delegate?.shapeSetView(self,
			                            didMoveTouchFrom: shapes[shapeID]!,
			                            withID: shapeID,
			                            by: CGSize(collection: displacementDelta))

			self.delegate?.shapeSetView(self,
																	didMoveTouchFrom: shapes[shapeID]!,
																	withID: shapeID,
																	displacement: CGSize(collection: touchLocation - initialTouchLocation))

			return (id: shapeID,
			        initialTouchLocation: initialTouchLocation,
			        previousTouchLocation: touchLocation)
		}
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesEnded(touches, with: event)

		guard let shapes = self.shapes else {
			return
		}

		touches.forEach {
			guard let shapeID = self.touchToShape[$0]?.id else {
				return
			}

			self.delegate?.shapeSetView(self,
			                            didTouchUpFrom: shapes[shapeID]!,
			                            withID: shapeID)
			self.touchToShape[$0] = nil
		}
	}
}
