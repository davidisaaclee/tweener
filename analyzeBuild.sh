#!/usr/bin/env bash

xcodebuild \
	-workspace Tweener.xcworkspace \
	-scheme Tweener \
	clean build \
	| grep [1-9].[0-9]ms \
	| sort -nr
	# > culprits.txt